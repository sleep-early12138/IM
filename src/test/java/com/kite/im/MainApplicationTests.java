package com.kite.im;

import javax.annotation.Resource;

import com.kite.im.utils.QrCodeUtils;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Base64;

/**
 * 主类测试

 */
@SpringBootTest
class MainApplicationTests implements Serializable {
    class User implements Serializable {
        private String name;
        private int age;

        public User(String name, int age) {
            this.name = name;
            this.age = age;
        }

        @Override
        public String toString() {
            return "User{" +
                    "name='" + name + '\'' +
                    ", age=" + age +
                    '}';
        }
    }
    @Test
    void testQR() {
        // 创建一个对象
        User user = new User("Alice", 25);

        // 生成带有对象数据的二维码
        byte[] qrCodeImageBase64 = QrCodeUtils.generateQrCode(user);
        System.out.println("QR Code Image Base64: " + qrCodeImageBase64);

        String encodedImage = Base64.getEncoder().encodeToString(qrCodeImageBase64);
        // 解析二维码获取对象数据
//        Object decodedData = QrCodeUtils.decodeQrCode(qrCodeImageBase64);
//        if (decodedData instanceof User) {
//            User decodedUser = (User) decodedData;
//            System.out.println("Decoded User: " + decodedUser.toString());
//        }
    }
}
