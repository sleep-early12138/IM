-- 创建数字证书表
CREATE TABLE IF NOT EXISTS `certificate` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `serial_number` varchar(64) NOT NULL COMMENT '证书序列号',
  `user_id` bigint(20) NOT NULL COMMENT '证书所有者ID',
  `user_name` varchar(64) NOT NULL COMMENT '证书所有者名称',
  `public_key` text NOT NULL COMMENT '证书公钥',
  `private_key` text COMMENT '证书私钥(仅CA根证书存储)',
  `signature` text NOT NULL COMMENT '证书签名',
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '证书状态(1:有效 0:吊销)',
  `valid_from` bigint(20) NOT NULL COMMENT '证书有效期开始时间',
  `valid_to` bigint(20) NOT NULL COMMENT '证书有效期结束时间',
  `create_time` bigint(20) NOT NULL COMMENT '创建时间',
  `update_time` bigint(20) NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_serial_number` (`serial_number`),
  UNIQUE KEY `idx_user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='数字证书表';

-- 创建好友关系表
-- auto-generated definition
create table friend_ship
(
    ship_id      bigint auto_increment comment '好友关系ID'
        primary key,
    user_id      bigint                             not null comment '用户ID',
    friend_id    bigint                             not null comment '好友ID',
    status       int                                not null comment '好友关系状态（正常0 or 黑名单1）',
    gmt_create   datetime default CURRENT_TIMESTAMP not null comment '申请创建时间',
    gmt_modified datetime default CURRENT_TIMESTAMP not null on update CURRENT_TIMESTAMP comment '申请更新时间'
) comment '好友关系表' collate = utf8mb4_unicode_ci;

-- 创建好友申请表
-- auto-generated definition
create table friend_request
(
    request_id   bigint auto_increment comment '申请ID'
        primary key,
    requester_id bigint                             not null comment '申请发起者ID',
    receiver_id  bigint                             not null comment '申请接收者ID',
    reason       varchar(255)                       null comment '申请理由',
    status       int                                not null comment '申请状态（例如：待处理、已通过、已拒绝等）',
    gmt_create   datetime default CURRENT_TIMESTAMP not null comment '申请创建时间',
    gmt_modified datetime default CURRENT_TIMESTAMP not null on update CURRENT_TIMESTAMP comment '申请更新时间'
) comment '好友申请表' collate = utf8mb4_unicode_ci;

-- auto-generated definition
create table message
(
    id                bigint auto_increment comment '消息ID'
        primary key,
    sender_id         bigint               not null comment '发送者ID',
    receiver_id       bigint               not null comment '接收者ID(用户ID或群ID)',
    content           text                 not null comment '消息内容',
    content_type      tinyint    default 0 not null comment '消息类型(0:文本 1:图片 2:语音 3:视频 4:文件 5:位置 6:表情)',
    conversation_type tinyint    default 0 not null comment '会话类型(0:单聊 1:群聊)',
    status            tinyint    default 0 not null comment '消息状态(0:未读 1:已读 2:撤回 3:删除)',
    send_time         bigint               not null comment '发送时间(时间戳)',
    read_time         bigint               null comment '已读时间(时间戳)',
    sequence          bigint               not null comment '消息序列号(用于消息排序和同步)',
    is_deleted        tinyint(1) default 0 not null comment '是否删除(0:未删除 1:已删除)',
    extra             varchar(1024)        null comment '扩展字段(JSON格式)',
    create_time       bigint               not null comment '创建时间',
    update_time       bigint               not null comment '更新时间'
)
    comment '消息表' charset = utf8mb4;

create index idx_receiver_status
    on message (receiver_id, status);

create index idx_send_time
    on message (send_time);

create index idx_sender_receiver
    on message (sender_id, receiver_id);

create index idx_sequence
    on message (sequence);



-- auto-generated definition
create table message_relation
(
    id              bigint auto_increment comment '关系ID'
        primary key,
    message_id      bigint               not null comment '消息ID',
    user_id         bigint               not null comment '用户ID',
    conversation_id varchar(64)          not null comment '会话ID',
    status          tinyint    default 0 not null comment '状态(0:未读 1:已读 2:删除)',
    is_sender       tinyint(1) default 0 not null comment '是否为发送者(0:否 1:是)',
    read_time       bigint               null comment '已读时间',
    create_time     bigint               not null comment '创建时间',
    update_time     bigint               not null comment '更新时间',
    constraint idx_message_user
        unique (message_id, user_id)
)
    comment '消息关系表' charset = utf8mb4;

create index idx_conversation_user
    on message_relation (conversation_id, user_id);

create index idx_user_status
    on message_relation (user_id, status);


-- auto-generated definition
create table conversation
(
    id                   varchar(64)       not null comment '会话ID'
        primary key,
    name                 varchar(128)      null comment '会话名称',
    type                 tinyint default 0 not null comment '会话类型(0:单聊 1:群聊)',
    owner_id             bigint            not null comment '会话所有者ID',
    target_id            bigint            not null comment '目标ID(用户ID或群ID)',
    last_message_id      bigint            null comment '最后一条消息ID',
    last_message_content varchar(255)      null comment '最后一条消息内容',
    last_message_time    bigint            null comment '最后一条消息时间',
    unread_count         int     default 0 not null comment '未读消息数',
    status               tinyint default 0 not null comment '会话状态(0:正常 1:置顶 2:免打扰 3:隐藏)',
    create_time          bigint            not null comment '创建时间',
    update_time          bigint            not null comment '更新时间'
)
    comment '会话表' charset = utf8mb4;

create index idx_owner_target
    on conversation (owner_id, target_id);

create index idx_owner_time
    on conversation (owner_id, last_message_time);


-- 创建系统通知表
CREATE TABLE system_notice (
                               notice_id bigint AUTO_INCREMENT COMMENT '通知的唯一标识',
                               sender_id bigint COMMENT '通知的发送者 ID，若为系统自动发送，此值可为空',
                               receiver_id bigint NOT NULL COMMENT '通知的接收者 ID',
                               notice_type varchar(50) NOT NULL COMMENT '通知的类型，如好友申请、系统公告、活动提醒等',
                               notice_title varchar(200) NOT NULL COMMENT '通知的标题',
                               notice_content text NOT NULL COMMENT '通知的具体内容',
                               send_time datetime DEFAULT CURRENT_TIMESTAMP COMMENT '通知的发送时间，默认值为当前时间',
                               read_status tinyint NOT NULL DEFAULT 0 COMMENT '通知的阅读状态，0 表示未读，1 表示已读',
                               click_status tinyint NOT NULL DEFAULT 0 COMMENT '通知的点击状态，0 表示未点击，1 表示已点击',
                               extra_info text COMMENT '额外信息，可用于存储与通知相关的其他数据，如跳转链接等，可为空',
                               create_time         datetime     default CURRENT_TIMESTAMP not null comment '创建时间',
                               update_time         datetime     default CURRENT_TIMESTAMP not null on update CURRENT_TIMESTAMP comment '更新时间',
                               isDelete           tinyint      default 0                 not null comment '是否删除',
                               PRIMARY KEY (notice_id)

) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT '系统通知表';

-- 在 receiver_id 上创建索引
CREATE INDEX idx_receiver_id ON system_notice (receiver_id);

-- 在 read_status 上创建索引
CREATE INDEX idx_read_status ON system_notice (read_status);

-- 创建联合索引，包含 receiver_id 和 read_status
CREATE INDEX idx_receiver_read_status ON system_notice (receiver_id, read_status);