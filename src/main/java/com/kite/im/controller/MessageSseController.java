package com.kite.im.controller;

import cn.dev33.satoken.stp.StpUtil;
import com.kite.im.model.vo.ChatMessageVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Slf4j
@RestController
@RequestMapping("/api/v1/messages")
public class MessageSseController {

    private static final Map<Long, SseEmitter> sseEmitters = new ConcurrentHashMap<>();

    /**
     * 建立SSE连接
     *
     * @return SseEmitter
     */
    @GetMapping(value = "/connect", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public SseEmitter connect(@RequestParam Long userId) {
        SseEmitter emitter = new SseEmitter(0L);

        // 注册回调
        emitter.onCompletion(() -> sseEmitters.remove(userId));
        emitter.onTimeout(() -> sseEmitters.remove(userId));
        emitter.onError(e -> sseEmitters.remove(userId));

        sseEmitters.put(userId, emitter);
        try {
            // 发送连接成功消息
            emitter.send(SseEmitter.event().name("connect").data("Connected!"));
        } catch (IOException e) {
            log.error("SSE连接异常", e);
        }
        return emitter;
    }

    /**
     * 发送消息给指定用户
     *
     * @param userId 用户ID
     * @param messageVO 消息内容
     */
    public static void sendMessage(Long userId, ChatMessageVO messageVO) {
        SseEmitter emitter = sseEmitters.get(userId);
        if (emitter != null) {
            try {
                emitter.send(SseEmitter.event()
                        .name("message")
                        .data(messageVO));
            } catch (IOException e) {
                log.error("发送消息失败", e);
                sseEmitters.remove(userId);
            }
        }
    }

    /**
     * 断开SSE连接
     */
    @GetMapping("/disconnect")
    public void disconnect(@RequestParam Long userId) {
        SseEmitter emitter = sseEmitters.remove(userId);
        if (emitter != null) {
            emitter.complete();
        }
    }
}