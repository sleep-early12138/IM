package com.kite.im.controller;

import com.kite.im.common.BaseResponse;
import com.kite.im.common.ErrorCode;
import com.kite.im.common.ResultUtils;
import com.kite.im.model.param.qun.ModifyQunParam;
import com.kite.im.model.param.qun.QunCreateParam;
import com.kite.im.model.vo.QunVO;
import com.kite.im.service.QunService;
import com.kite.im.service.UserService;
import com.kite.im.service.impl.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author: lizhouwei
 * @ date : 2023/6/20
 * @description: 群聊相关控制器
 */
@RestController
@RequestMapping("/qun")
public class QunController {

    @Autowired
    private QunService qunService;

    /**
     * 创建群聊
     *
     * @param qunCreateParam
     * @return
     */
    @PostMapping("/createQun")
    public BaseResponse<Long> newQun(@RequestBody QunCreateParam qunCreateParam) {
        return ResultUtils.success(qunService.newQun(qunCreateParam));
    }

    /**
     * 修改群聊
     *
     * @param modifyQunParam
     * @return
     */
    @PostMapping("/modifyQun")
    public BaseResponse<Long> modifyQun(@RequestBody ModifyQunParam modifyQunParam) {
        return ResultUtils.success(qunService.modifyQun(modifyQunParam));
    }

    /**
     * 查询群聊详情
     *
     * @param qunId
     * @return
     */
    @GetMapping("/queryQunDetail")
    public BaseResponse<QunVO> queryQunDetail(@RequestParam Long qunId) {
        QunVO qunVO = qunService.queryQunDetail(qunId);
        if (qunVO == null) {
            return ResultUtils.error(ErrorCode.NOT_FOUND_ERROR, "没有找到群聊");
        }
        return ResultUtils.success(qunVO);
    }

    /**
     * 查询群聊列表
     *
     * @param categoryId
     * @return
     * @Description: 根据群聊列表查询
     */
    @GetMapping("/queryQunList")
    public BaseResponse<List<QunVO>> queryQunList(@RequestParam Long categoryId) {
        List<QunVO> qunVOList = qunService.queryQunList(categoryId);
        if (qunVOList == null) {
            return ResultUtils.error(ErrorCode.NOT_FOUND_ERROR, "没有找到群聊");
        }
        return ResultUtils.success(qunVOList);
    }


    /**
     * 查询我创建的群聊列表
     *
     * @param request
     * @return
     */
    @GetMapping("/queryMyCreateQunList")
    public BaseResponse<List<QunVO>> queryMyCreateQunList(HttpServletRequest request) {
        List<QunVO> qunVOList = qunService.queryMyCreateQunList(request);
        if (qunVOList == null) {
            return ResultUtils.error(ErrorCode.NOT_FOUND_ERROR, "您还没有创建任何群聊");
        }
        return ResultUtils.success(qunVOList);
    }

    /**
     * 查询我加入的群聊列表
     *
     * @param request
     * @return
     */
    @GetMapping("/queryMyJoinQunList")
    public BaseResponse<List<QunVO>> queryMyJoinQunList(HttpServletRequest request) {
        List<QunVO> qunVOList = qunService.queryMyJoinQunList(request);
        if (qunVOList == null) {
            return ResultUtils.error(ErrorCode.NOT_FOUND_ERROR, "您还没有加入任何群聊");
        }
        return ResultUtils.success(qunVOList);
    }

    /**
     * 解散群聊
     */
    @PostMapping("/dismissQun")
    public BaseResponse<Object> dismissQun(Long qunId,HttpServletRequest request) {
        return ResultUtils.success(qunService.dismissQun(qunId,request));
    }

}
