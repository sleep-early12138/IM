package com.kite.im.controller;

import com.kite.im.common.BaseResponse;
import com.kite.im.common.ResultUtils;
import com.kite.im.model.entity.User;
import com.kite.im.model.param.qun.InviteFriendParam;
import com.kite.im.model.param.qun.RemoveMemberParam;
import com.kite.im.model.vo.QunMemberVO;
import com.kite.im.model.vo.UserVO;
import com.kite.im.service.QunMemberService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author: lizhouwei
 * @ date : 2023/6/23
 * @description:
 */

@RestController
@RequestMapping("/member")
public class QunMemberController {


    @Resource
    private QunMemberService qunMemberService;
    /**
     * 退出群聊
     */

    @PostMapping("/quitQun")
    public BaseResponse<Object> quitQun(Long qunId,HttpServletRequest request) {
        return ResultUtils.success(qunMemberService.quitQun(qunId,request));
    }

    /**
     * 踢出群聊
     */
    @PostMapping("/kickOutQun")
    public BaseResponse<Object> kickOutQun(RemoveMemberParam removeMemberParam) {
        return ResultUtils.success(qunMemberService.kickOutQun(removeMemberParam));
    }

    /**
     * 群主转让
     */
    @PostMapping("/transferQun")
    public BaseResponse<Object> transferQun(Long qunId, Long newOwner) {
        return ResultUtils.success(qunMemberService.transferQun(qunId,newOwner));
    }




    /**
     * 查询群成员列表
     */
    @GetMapping("/queryQunMemberList")
    public BaseResponse<List<QunMemberVO>> queryQunMemberList(Long qunId) {
        return ResultUtils.success(qunMemberService.queryQunMemberList(qunId));
    }


}
