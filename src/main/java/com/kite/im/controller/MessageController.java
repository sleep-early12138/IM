package com.kite.im.controller;

import cn.dev33.satoken.stp.StpUtil;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.kite.im.common.BaseResponse;
import com.kite.im.common.ResultUtils;
import com.kite.im.model.entity.Message;
import com.kite.im.service.MessageService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@Slf4j
@RestController
@RequestMapping("/messages")
public class MessageController {

    @Autowired
    private MessageService messageService;

    /**
     * 发送消息
     *
     * @param message
     * @param request
     * @return
     */
    @PostMapping("/send")
    public BaseResponse<Boolean> sendMessage(@RequestBody Message message, HttpServletRequest request) {
        message.setSenderId(StpUtil.getLoginIdAsLong());
        messageService.sendMessage(message);
        return ResultUtils.success(true);
    }

    /**
     * 获取消息历史
     *
     * @param conversationId
     * @param lastMessageId
     * @param pageSize
     * @param request
     * @return
     */
    @GetMapping("/history")
    public BaseResponse<Page<Message>> getMessageHistory(
            @RequestParam String conversationId,
            @RequestParam(required = false) Long lastMessageId,
            @RequestParam(defaultValue = "20") Integer pageSize,
            HttpServletRequest request) {
        Page<Message> messagePage = messageService.getMessageHistory(conversationId, StpUtil.getLoginIdAsLong(), lastMessageId, pageSize);
        return ResultUtils.success(messagePage);
    }

    /**
     * 标记消息已读
     *
     * @param conversationId
     * @param request
     * @return
     */
    @PostMapping("/read")
    public BaseResponse<Boolean> markMessageRead(
            @RequestParam String conversationId,
            HttpServletRequest request) {
        messageService.markMessageRead(conversationId, StpUtil.getLoginIdAsLong());
        return ResultUtils.success(true);
    }

    /**
     * 撤回消息
     *
     * @param messageId
     * @param request
     * @return
     */
    @PostMapping("/recall/{messageId}")
    public BaseResponse<Boolean> recallMessage(
            @PathVariable Long messageId,
            HttpServletRequest request) {
        messageService.recallMessage(messageId, StpUtil.getLoginIdAsLong());
        return ResultUtils.success(true);
    }

    /**
     * 删除消息
     *
     * @param messageId
     * @param request
     * @return
     */
    @DeleteMapping("/{messageId}")
    public BaseResponse<Boolean> deleteMessage(
            @PathVariable Long messageId,
            HttpServletRequest request) {
        messageService.deleteMessage(messageId, StpUtil.getLoginIdAsLong());
        return ResultUtils.success(true);
    }

    /**
     * 获取未读消息数
     *
     * @param request
     * @return
     */
    @GetMapping("/unread/count")
    public BaseResponse<Integer> getUnreadCount(HttpServletRequest request) {
        return ResultUtils.success(messageService.getUnreadCount(StpUtil.getLoginIdAsLong()));
    }
} 