package com.kite.im.controller;

import com.kite.im.common.BaseResponse;
import com.kite.im.common.ResultUtils;
import com.kite.im.model.dto.FriendRequestDTO;
import com.kite.im.model.entity.FriendRequest;
import com.kite.im.model.param.audit.QueryFriendParam;
import com.kite.im.model.query.ContactQuery;
import com.kite.im.service.ContactService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;


/**
 * @author: lizhouwei
 * @ date : 2023/5/29
 * @description: 联系人
 */
@RestController
@RequestMapping("/contact")
public class ContactController {

    @Autowired
    private ContactService contactService;


    /**
     * 查找好友
     *
     * @param userAccount
     * @return
     */
    @GetMapping("/queryByEmail")
    public BaseResponse<String> queryByEmail(@RequestParam String userAccount,HttpServletRequest request) {
        QueryFriendParam friendParam = contactService.queryByUserAccount(userAccount,request);
        return ResultUtils.success(friendParam.getFriendSecretIdentify());
    }

    /**
     * 发起好友申请
     *
     * @param friendSecretIdentify
     * @param reason
     * @return
     */
    @PostMapping("/requestFriend")
    public BaseResponse<Long> RequestFriend(@RequestBody FriendRequestDTO requestDTO, HttpServletRequest request) {
        Long friendParam = contactService.requestFriend(requestDTO.getFriendSecretIdentify(), requestDTO.getReason(), request);
        return ResultUtils.success(friendParam);
    }


    /**
     * 审核好友申请
     *
     * @param requestId
     * @return
     */
    @PostMapping("/auditRequestFriend")
    public BaseResponse<Integer> RequestFriendAudit(@RequestBody FriendRequestDTO requestDTO, HttpServletRequest request) {

        int friendAudit = contactService.requestFriendAudit(requestDTO.getRequestId(), requestDTO.getAuditId(), request);
        return ResultUtils.success(friendAudit);
    }

    /**
     * 查询好友联系人列表
     *
     * @return
     */
    @GetMapping("/queryFriendList")
    public BaseResponse<List<ContactQuery>> queryFriendList(HttpServletRequest request) {
       List<ContactQuery> friendList = contactService.queryFriendList(request);
        return ResultUtils.success(friendList);
    }

    /**
     * 删除好友
     *
     * @param friendId
     * @return
     */
    @PostMapping("/deleteFriend")
    public BaseResponse<Integer> deleteFriend(@RequestParam Long friendId, HttpServletRequest request) {
        int friendAudit = contactService.deleteFriend(friendId, request);
        return ResultUtils.success(friendAudit);
    }

    /**
     * 查询好友申请列表
     *
     * @return
     */
    @GetMapping("/queryFriendRequestList")
    public BaseResponse<List<FriendRequest>> queryFriendRequestList(HttpServletRequest request) {
       List<FriendRequest> friendList = contactService.queryFriendRequestList(request);
        return ResultUtils.success(friendList);
    }

}
