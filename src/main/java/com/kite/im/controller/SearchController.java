package com.kite.im.controller;

import com.kite.im.common.BaseResponse;
import com.kite.im.common.ResultUtils;
import com.kite.im.model.vo.SearchResultVO;
import com.kite.im.service.SearchService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/search")
public class SearchController {

    @Autowired
    private SearchService searchService;

    /**
     * 聚合搜索
     *
     * @param keyword 搜索关键词
     * @param type 搜索类型（0-联系人，1-会话，2-消息，3-全部）
     * @param request
     * @return
     */
    @GetMapping
    public BaseResponse<List<SearchResultVO>> search(
            @RequestParam String keyword,
            @RequestParam(defaultValue = "3") Integer type,
            HttpServletRequest request) {
        List<SearchResultVO> searchResults = searchService.search(keyword, type);
        return ResultUtils.success(searchResults);
    }
} 