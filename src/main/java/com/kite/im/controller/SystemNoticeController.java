package com.kite.im.controller;

import cn.dev33.satoken.stp.StpUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.kite.im.common.BaseResponse;
import com.kite.im.common.ResultUtils;
import com.kite.im.model.entity.SystemNotice;
import com.kite.im.service.SystemNoticeService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * 系统通知控制器
 */
@RestController
@RequestMapping("/api/notice")
public class SystemNoticeController {

    @Resource
    private SystemNoticeService systemNoticeService;

    /**
     * 创建系统通知
     *
     * @param systemNotice 系统通知信息
     * @return 创建结果
     */
    @PostMapping("/create")
    public BaseResponse<Boolean> createNotice(@RequestBody SystemNotice systemNotice) {
        return ResultUtils.success(systemNoticeService.createNotice(systemNotice));
    }

    /**
     * 更新系统通知
     *
     * @param systemNotice 系统通知信息
     * @return 更新结果
     */
    @PutMapping("/update")
    public BaseResponse<Boolean> updateNotice(@RequestBody SystemNotice systemNotice) {
        return ResultUtils.success(systemNoticeService.updateNotice(systemNotice));
    }

    /**
     * 删除系统通知
     *
     * @param noticeId 通知ID
     * @return 删除结果
     */
    @DeleteMapping("/delete/{noticeId}")
    public BaseResponse<Boolean> deleteNotice(@PathVariable Long noticeId) {
        return ResultUtils.success(systemNoticeService.deleteNotice(noticeId));
    }

    /**
     * 批量删除系统通知
     *
     * @param noticeIds 通知ID列表
     * @return 删除结果
     */
    @DeleteMapping("/batch/delete")
    public BaseResponse<Boolean> batchDeleteNotices(@RequestBody List<Long> noticeIds) {
        return ResultUtils.success(systemNoticeService.batchDeleteNotices(noticeIds));
    }

    /**
     * 根据ID获取系统通知
     *
     * @param noticeId 通知ID
     * @return 系统通知信息
     */
    @GetMapping("/get/{noticeId}")
    public BaseResponse<SystemNotice> getNoticeById(@PathVariable Long noticeId) {
        return ResultUtils.success(systemNoticeService.getNoticeById(noticeId));
    }

    /**
     * 分页获取用户的系统通知
     *
     * @param receiverId 接收者ID
     * @param page 页码
     * @param size 每页大小
     * @return 分页的系统通知列表
     */
    @GetMapping("/list")
    public BaseResponse<IPage<SystemNotice>> getUserNotices(
            @RequestParam(defaultValue = "1") long page,
            @RequestParam(defaultValue = "10") long size) {
        Long receiverId = Long.parseLong(StpUtil.getLoginId().toString());
        return ResultUtils.success(systemNoticeService.getUserNotices(receiverId, page, size));
    }

    /**
     * 将通知标记为已读
     *
     * @param noticeId 通知ID
     * @return 更新结果
     */
    @PutMapping("/read/{noticeId}")
    public BaseResponse<Boolean> markAsRead(@PathVariable Long noticeId) {
        return ResultUtils.success(systemNoticeService.markAsRead(noticeId));
    }

    /**
     * 批量将通知标记为已读
     *
     * @param noticeIds 通知ID列表
     * @return 更新结果
     */
    @PutMapping("/batch/read")
    public BaseResponse<Boolean> batchMarkAsRead(@RequestBody List<Long> noticeIds) {
        return ResultUtils.success(systemNoticeService.batchMarkAsRead(noticeIds));
    }

    /**
     * 获取用户未读通知数量
     *
     * @param receiverId 接收者ID
     * @return 未读通知数量
     */
    @GetMapping("/unread/count")
    public BaseResponse<Long> getUnreadCount() {
        Long receiverId = Long.parseLong(StpUtil.getLoginId().toString());
        return ResultUtils.success(systemNoticeService.getUnreadCount(receiverId));
    }
}