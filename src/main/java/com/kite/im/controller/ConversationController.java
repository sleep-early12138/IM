package com.kite.im.controller;

import cn.dev33.satoken.stp.StpUtil;
import com.kite.im.common.BaseResponse;
import com.kite.im.common.ResultUtils;
import com.kite.im.model.dto.ConversionDTO;
import com.kite.im.model.entity.Conversation;
import com.kite.im.model.vo.ConversationVO;
import com.kite.im.service.ConversationService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/conversations")
public class ConversationController {

    @Resource
    private ConversationService conversationService;

    /**
     * 获取会话列表
     *
     * @param request
     * @return
     */
    @GetMapping("/list")
    public BaseResponse<List<ConversationVO>> getConversationList(HttpServletRequest request) {
        List<ConversationVO> conversationList = conversationService.getConversationList(StpUtil.getLoginIdAsLong());
        return ResultUtils.success(conversationList);
    }

    /**
     * 创建或获取会话
     *
     * @param targetId
     * @param type
     * @param request
     * @return
     */
    @PostMapping("/create")
    public BaseResponse<Conversation> createConversation(
            @RequestBody ConversionDTO conversionDTO,
            HttpServletRequest request) {
        Conversation conversation = conversationService.createOrGetConversation(StpUtil.getLoginIdAsLong(), conversionDTO.getTargetId(), conversionDTO.getType());
        return ResultUtils.success(conversation);
    }

    /**
     * 删除会话
     *
     * @param conversationId
     * @param request
     * @return
     */
    @DeleteMapping("/{conversationId}")
    public BaseResponse<Boolean> deleteConversation(
            @PathVariable String conversationId,
            HttpServletRequest request) {
        conversationService.deleteConversation(conversationId, StpUtil.getLoginIdAsLong());
        return ResultUtils.success(true);
    }

    /**
     * 置顶会话
     *
     * @param conversationId
     * @param request
     * @return
     */
    @PostMapping("/{conversationId}/pin")
    public BaseResponse<Boolean> pinConversation(
            @PathVariable String conversationId,
            HttpServletRequest request) {
        conversationService.pinConversation(conversationId, StpUtil.getLoginIdAsLong());
        return ResultUtils.success(true);
    }

    /**
     * 取消置顶会话
     *
     * @param conversationId
     * @param request
     * @return
     */
    @PostMapping("/{conversationId}/unpin")
    public BaseResponse<Boolean> unpinConversation(
            @PathVariable String conversationId,
            HttpServletRequest request) {
        conversationService.unpinConversation(conversationId, StpUtil.getLoginIdAsLong());
        return ResultUtils.success(true);
    }
} 