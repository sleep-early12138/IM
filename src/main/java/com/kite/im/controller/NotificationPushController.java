package com.kite.im.controller;

import com.kite.im.service.SystemNoticeService;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

import java.io.IOException;
import java.util.concurrent.ConcurrentHashMap;

@RestController
@RequestMapping("/api/v1/notifications")
public class NotificationPushController {

    private final SystemNoticeService systemNoticeService;
    private final ConcurrentHashMap<Long, SseEmitter> sseEmitters = new ConcurrentHashMap<>();

    public NotificationPushController(SystemNoticeService systemNoticeService) {
        this.systemNoticeService = systemNoticeService;
    }

    @GetMapping(value = "/subscribe", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public SseEmitter subscribe(@RequestParam Long userId) {
        SseEmitter emitter = new SseEmitter(0L); // 0表示不过期

        // 设置超时和完成时的回调，确保资源被正确释放
        emitter.onTimeout(() -> sseEmitters.remove(userId));
        emitter.onCompletion(() -> sseEmitters.remove(userId));

        // 存储用户的SSE连接
        sseEmitters.put(userId, emitter);

        // 发送初始连接成功消息
        try {
            emitter.send(SseEmitter.event()
                    .name("connect")
                    .data("连接成功"));
        } catch (IOException e) {
            sseEmitters.remove(userId);
            return null;
        }

        return emitter;
    }

    @GetMapping("/unsubscribe")
    public void unsubscribe(@RequestParam Long userId) {
        SseEmitter emitter = sseEmitters.remove(userId);
        if (emitter != null) {
            emitter.complete();
        }
    }

    // 向指定用户推送通知
    public void pushNotification(Long userId, Object notification) {
        SseEmitter emitter = sseEmitters.get(userId);
        if (emitter != null) {
            try {
                emitter.send(SseEmitter.event()
                        .name("notification")
                        .data(notification));
            } catch (IOException e) {
                sseEmitters.remove(userId);
            }
        }
    }
}