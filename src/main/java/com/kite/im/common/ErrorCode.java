package com.kite.im.common;

/**
 * 错误码
 *
 * @author lizhouwei
 */
public enum ErrorCode {

    SUCCESS(0, "ok", ""),
    PARAMS_ERROR(40000, "请求参数错误", ""),
    PARAMS_ERROR_EMAIL_REPATE(40000, "账号重复", ""),
    PARAMS_ERROR_NUL(40000, "请求参数不完整", ""),
    PARAMS_ERROR_EMAIL_SHORT(40000, "用户账户过短", ""),
    PARAMS_ERROR_PASS_SHORT(40000, "用户密码过短", ""),
    Repate_PARAMS_ERROR(40010, "账号重复", ""),
    NULL_ERROR(40001, "请求数据为空", ""),
    NOT_LOGIN_ERROR(40100, "未登录", ""),
    NO_AUTH_ERROR(40101, "无权限", ""),
    SYSTEM_ERROR(50000, "系统内部异常", ""),
    PARAMS_ERROR_EMAIL_FORMAT(40000, "邮箱格式错误","" ),
    NOT_FOUND_ERROR(40400, "请求数据不存在", ""),
    OPERATION_ERROR(50001, "操作失败", "");


    private final int code;

    /**
     * 状态码信息
     */
    private final String message;

    /**
     * 状态码描述（详情）
     */
    private final String description;

    ErrorCode(int code, String message, String description) {
        this.code = code;
        this.message = message;
        this.description = description;
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public String getDescription() {
        return description;
    }
}
