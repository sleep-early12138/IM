package com.kite.im;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.data.redis.RedisAutoConfiguration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * 主类（项目启动入口）
 */

/**
 * `@EnableScheduling` 注解用于启用 Spring 的定时任务调度功能。通过添加这个注解，您可以在 Spring 应用程序中使用 `@Scheduled` 注解来定义定时任务的执行规则。这样，您就可以轻松地创建定时任务并配置它们的执行时间、频率等。
 *
 * `@EnableAspectJAutoProxy` 注解用于启用基于 AspectJ 的自动代理功能。AspectJ 是一个强大的面向切面编程（AOP）框架，允许开发者将横切关注点（cross-cutting concerns）与核心业务逻辑进行解耦。通过添加这个注解，Spring 将会基于 AspectJ 的语法和规则来自动创建代理对象，实现切面的织入。
 *
 * `proxyTargetClass = true` 参数表示使用 CGLIB 动态代理来创建代理对象。默认情况下，Spring 使用 JDK 动态代理来实现接口的代理，但是如果目标对象没有实现任何接口，则需要将 `proxyTargetClass` 设置为 true，以便使用 CGLIB 动态代理。
 *
 * `exposeProxy = true` 参数表示将代理对象暴露给 AOP 代理链中的其他切面。这样，在同一切面中的不同通知方法之间可以共享代理对象，从而实现更灵活的切面编程。
 *
 * 总结起来，`@EnableScheduling` 注解用于启用 Spring 的定时任务调度功能，而 `@EnableAspectJAutoProxy` 注解用于启用基于 AspectJ 的自动代理功能，实现面向切面编程。
 */
//  如需开启 Redis，须移除 exclude 中的内容
@SpringBootApplication(exclude = {RedisAutoConfiguration.class})
@MapperScan("com.kite.im.mapper")
@EnableScheduling
@EnableAspectJAutoProxy(proxyTargetClass = true, exposeProxy = true)
public class MainApplication {

    public static void main(String[] args) {
        SpringApplication.run(MainApplication.class, args);
    }

}
