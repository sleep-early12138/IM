    package com.kite.im.websocket;

    import cn.dev33.satoken.stp.StpUtil;
    import com.kite.im.constant.MessageConstant;
    import com.kite.im.model.dto.ChatMessageDTO;
    import com.kite.im.model.entity.Message;
    import com.kite.im.model.vo.ChatMessageVO;
    import com.kite.im.service.ConversationService;
    import com.kite.im.service.MessageService;
    import com.kite.im.utils.JSONUtil;
    import lombok.extern.slf4j.Slf4j;
    import org.springframework.stereotype.Component;

    import javax.websocket.*;
    import javax.websocket.server.ServerEndpoint;
    import java.io.IOException;
    import java.util.Map;
    import java.util.concurrent.ConcurrentHashMap;

    @Slf4j
    @Component
    @ServerEndpoint("/ws/chat")
    public class WebSocketServer {

        private static final Map<Long, Session> ONLINE_SESSIONS = new ConcurrentHashMap<>();

        private static MessageService messageService;
        private static ConversationService conversationService;

        private Long userId;
        private Session session;

        public static void setMessageService(MessageService service) {
            messageService = service;
        }

        public static void setConversationService(ConversationService service) {
            conversationService = service;
        }

        @OnOpen
        public void onOpen(Session session) {
            try {
                this.userId = StpUtil.getLoginIdAsLong();
                this.session = session;
                ONLINE_SESSIONS.put(userId, session);

                // 发送上线通知
                sendSystemMessage(userId, "用户上线");
                log.info("用户连接成功 - userId: {}, 当前在线人数: {}", userId, ONLINE_SESSIONS.size());
            } catch (Exception e) {
                log.error("WebSocket连接异常", e);
                try {
                    session.close();
                } catch (IOException ex) {
                    log.error("关闭WebSocket连接异常", ex);
                }
            }
        }

        @OnClose
        public void onClose() {
            if (this.userId != null) {
                ONLINE_SESSIONS.remove(this.userId);
                // 发送下线通知
                sendSystemMessage(userId, "用户下线");
                log.info("用户断开连接 - userId: {}, 当前在线人数: {}", userId, ONLINE_SESSIONS.size());
            }
        }

        @OnMessage
        public void onMessage(String messageText) {
            log.info("收到消息 - userId: {}, message: {}", this.userId, messageText);
            try {
                ChatMessageDTO messageDTO = JSONUtil.parseObject(messageText, ChatMessageDTO.class);

                // 1. 检查会话是否存在
                if (!conversationService.getById(messageDTO.getConversationId()).getOwnerId().equals(userId)) {
                    sendErrorMessage("无权发送消息到此会话");
                    return;
                }

                // 2. 构建消息实体
                Message message = new Message();
                message.setSenderId(this.userId);
                message.setReceiverId(messageDTO.getReceiverId());
                message.setContent(messageDTO.getContent());
                message.setContentType(messageDTO.getContentType());
                message.setConversationType(MessageConstant.CONVERSATION_TYPE_SINGLE);
                message.setExtra(messageDTO.getExtra());

                // 3. 保存并发送消息
                messageService.sendMessage(message);

                // 4. 构建返回消息
                ChatMessageVO messageVO = new ChatMessageVO();
                messageVO.setMessageId(message.getId());
                messageVO.setType(messageDTO.getType());
                messageVO.setConversationId(messageDTO.getConversationId());
                messageVO.setSenderId(this.userId);
                messageVO.setReceiverId(messageDTO.getReceiverId());
                messageVO.setContent(messageDTO.getContent());
                messageVO.setContentType(messageDTO.getContentType());
                messageVO.setSendTime(message.getSendTime());
                messageVO.setExtra(messageDTO.getExtra());
                messageVO.setSequence(message.getSequence());

                // 5. 发送消息给接收者
                sendToUser(messageDTO.getReceiverId(), JSONUtil.toJSONString(messageVO));

            } catch (Exception e) {
                log.error("处理消息异常", e);
                sendErrorMessage("消息处理失败：" + e.getMessage());
            }
        }

        @OnError
        public void onError(Session session, Throwable error) {
            log.error("WebSocket异常 - userId: " + this.userId, error);
            sendErrorMessage("连接异常：" + error.getMessage());
        }

        /**
         * 发送消息给指定用户
         */
        private void sendToUser(Long targetUserId, String message) {
            Session targetSession = ONLINE_SESSIONS.get(targetUserId);
            if (targetSession != null && targetSession.isOpen()) {
                try {
                    targetSession.getBasicRemote().sendText(message);
                } catch (IOException e) {
                    log.error("发送消息失败 - targetUserId: " + targetUserId, e);
                }
            }
        }

        /**
         * 发送系统消息
         */
        private void sendSystemMessage(Long userId, String content) {
            ChatMessageVO systemMessage = new ChatMessageVO();
            systemMessage.setType(2); // 系统消息
            systemMessage.setContent(content);
            systemMessage.setSendTime(System.currentTimeMillis());

            ONLINE_SESSIONS.values().forEach(session -> {
                if (session.isOpen()) {
                    try {
                        session.getBasicRemote().sendText(JSONUtil.toJSONString(systemMessage));
                    } catch (IOException e) {
                        log.error("发送系统消息失败", e);
                    }
                }
            });
        }

        /**
         * 发送错误消息
         */
        private void sendErrorMessage(String errorMessage) {
            if (this.session != null && this.session.isOpen()) {
                ChatMessageVO errorVO = new ChatMessageVO();
                errorVO.setType(3); // 错误消息
                errorVO.setContent(errorMessage);
                errorVO.setSendTime(System.currentTimeMillis());

                try {
                    this.session.getBasicRemote().sendText(JSONUtil.toJSONString(errorVO));
                } catch (IOException e) {
                    log.error("发送错误消息失败", e);
                }
            }
        }

        /**
         * 检查用户是否在线
         */
        public static boolean isOnline(Long userId) {
            return ONLINE_SESSIONS.containsKey(userId);
        }

        /**
         * 发送消息给指定用户（供其他服务调用）
         */
        public static void sendMessage(Long userId, String message) {
            Session session = ONLINE_SESSIONS.get(userId);
            if (session != null && session.isOpen()) {
                try {
                    session.getBasicRemote().sendText(message);
                } catch (IOException e) {
                    log.error("发送消息失败 - userId: {}, error: {}", userId, e.getMessage());
                }
            }
        }
    }