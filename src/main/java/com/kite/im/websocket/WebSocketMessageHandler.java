package com.kite.im.websocket;

import cn.dev33.satoken.stp.StpUtil;
import com.kite.im.model.entity.Message;
import com.kite.im.service.MessageService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;

import java.time.LocalDateTime;

@Slf4j
@Controller
public class WebSocketMessageHandler {

    @Autowired
    private SimpMessagingTemplate messagingTemplate;

    @Autowired
    private MessageService messageService;

    /**
     * 处理客户端发送的消息
     *
     * @param message        消息内容
     * @param headerAccessor 消息头访问器
     */
    @MessageMapping("/chat.sendMessage")
    public void handleChatMessage(@Payload Message message, SimpMessageHeaderAccessor headerAccessor) {
        try {
            // 设置发送者ID
            message.setSenderId(StpUtil.getLoginIdAsLong());

            message.setSendTime(System.currentTimeMillis());


            // 保存消息到数据库
            messageService.sendMessage(message);

            // 发送消息到指定用户
            messagingTemplate.convertAndSendToUser(
                    message.getReceiverId().toString(),
                    "/queue/messages",
                    message
            );

            log.info("Message sent successfully: {}", message);
        } catch (Exception e) {
            log.error("Error handling chat message: {}", e.getMessage(), e);
        }
    }

    /**
     * 处理用户加入聊天
     *
     * @param headerAccessor 消息头访问器
     */
    @MessageMapping("/chat.addUser")
    public void handleUserJoin(SimpMessageHeaderAccessor headerAccessor) {
        try {
            String userId = StpUtil.getLoginIdAsString();
            // 将用户ID添加到WebSocket会话属性中
            headerAccessor.getSessionAttributes().put("userId", userId);
            log.info("User joined: {}", userId);
        } catch (Exception e) {
            log.error("Error handling user join: {}", e.getMessage(), e);
        }
    }
}