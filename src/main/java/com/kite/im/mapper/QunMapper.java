package com.kite.im.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.kite.im.model.entity.Qun;
import com.kite.im.model.param.qun.QunCreateParam;
import com.kite.im.model.vo.QunVO;

import java.util.List;

/**
* @author lizhouwei
* @description 针对表【qun(qun)】的数据库操作Mapper
* @createDate 2023-06-20 20:26:25
* @Entity com.kite.im.model.domain.Qun
*/
public interface QunMapper extends BaseMapper<Qun> {

    int insert(QunCreateParam qunCreateParam);

    Qun queryQunDetail(Long qunId);

    List<Qun> queryQunList(Long categoryId);

    List<Qun> queryMyCreateQunList(Long userId);


    List<Qun> queryMyJoinQunList(Long userId);

    int deleteQun(Long qunId);

    int updateOwn(Long qunId, Long newOwner);
}




