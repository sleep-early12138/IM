package com.kite.im.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.kite.im.model.entity.Event;

/**
* @author lizhouwei
* @description 针对表【event(event)】的数据库操作Mapper
* @createDate 2023-06-20 20:26:25
* @Entity com.kite.im.model.domain.Event
*/
public interface EventMapper extends BaseMapper<Event> {

}




