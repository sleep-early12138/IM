package com.kite.im.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.kite.im.model.entity.SystemNotice;
import org.apache.ibatis.annotations.Mapper;

/**
 * 系统通知表 Mapper 接口
 */
@Mapper
public interface SystemNoticeMapper extends BaseMapper<SystemNotice> {
}