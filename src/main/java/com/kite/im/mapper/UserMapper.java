package com.kite.im.mapper;

import com.kite.im.model.entity.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.kite.im.model.query.ContactQuery;

/**
* @author lizhouwei
* @description 针对表【user(用户)】的数据库操作Mapper
* @createDate 2023-05-19 16:20:08
* @Entity com.kite.im.model.entity.User
*/
public interface UserMapper extends BaseMapper<User> {

    User queryByUserAccount(String userAccount);

    ContactQuery queryFriendList(Long id);

    User queryById(Long memberId);
}




