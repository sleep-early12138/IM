package com.kite.im.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.kite.im.model.entity.QunMember;

import java.util.List;

/**
* @author lizhouwei
* @description 针对表【qun_member(qun_member)】的数据库操作Mapper
* @createDate 2023-06-20 20:26:25
* @Entity com.kite.im.model.domain.QunMember
*/
public interface QunMemberMapper extends BaseMapper<QunMember> {

    int save(Long qunId, Long userId);

    QunMember queryQunMemberByQunIdAndUserId(Long qunId, Long memberId);

    int addMemberToQun(Long qunId, Long memberId);

    int deleteQunMember(Long qunId);

    int deleteMember(Long qunId, Long userId);


    List<QunMember> queryQunMemberList(Long qunId);
}




