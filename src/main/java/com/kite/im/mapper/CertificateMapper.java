package com.kite.im.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.kite.im.model.entity.Certificate;
import org.apache.ibatis.annotations.Mapper;

/**
 * 数字证书Mapper接口
 */
@Mapper
public interface CertificateMapper extends BaseMapper<Certificate> {
    // 继承BaseMapper后已包含基本的CRUD方法
}