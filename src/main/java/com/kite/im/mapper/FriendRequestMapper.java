package com.kite.im.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.kite.im.model.entity.FriendRequest;
import com.kite.im.model.entity.User;
import com.kite.im.model.param.audit.FriendRequestParam;

import java.util.List;

/**
* @author lizhouwei
* @description 针对表【friend_request(好友申请表)】的数据库操作Mapper
* @createDate 2023-05-29 19:36:54
* @Entity com.kite.im.model.domain.FriendRequest
*/
public interface FriendRequestMapper extends BaseMapper<FriendRequest> {

    int updateStatus(Long requestId, Integer status);

    int addFriend(FriendRequestParam friendRequest);

    boolean exist(Long requestId);

    FriendRequest querytById(Long requestId);

    List<FriendRequest> queryFriendRequestList(Long receiverId);


}




