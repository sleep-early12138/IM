package com.kite.im.mapper;

import com.kite.im.model.entity.MessageRelation;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface MessageRelationMapper extends BaseMapper<MessageRelation> {
} 