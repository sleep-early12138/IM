package com.kite.im.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.kite.im.model.entity.FriendShip;

import java.util.List;

/**
* @author lizhouwei
* @description 针对表【friend_ship(好友关系表)】的数据库操作Mapper
* @createDate 2023-05-29 19:43:10
* @Entity com.kite.im.model.domain.FriendShip
*/
public interface FriendShipMapper extends BaseMapper<FriendShip> {

    List<Long> queryFriendList(Long userId);

    int addFriend(FriendShip friendShip);

    int deleteFriend(Long userId, Long friendId, Integer status);

    boolean existFriend(Long userId,Long receiverId);

    FriendShip queryFriendById(Long userId,Long friendId);
}




