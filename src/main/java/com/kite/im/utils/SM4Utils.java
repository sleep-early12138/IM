package com.kite.im.utils;


import cn.hutool.core.util.RandomUtil;
import cn.hutool.crypto.SmUtil;
import cn.hutool.crypto.symmetric.SM4;
import lombok.extern.slf4j.Slf4j;

import java.nio.charset.StandardCharsets;
import java.util.Base64;

/**
 * SM4对称加密工具类
 * SM4是一种分组密码算法，分组长度为128位，密钥长度为128位
 */
@Slf4j
public class SM4Utils {

    /**
     * 生成随机SM4密钥
     * @return 16字节的随机密钥(Base64编码)
     */
    public static String generateKey() {
        // SM4密钥长度为128位，即16字节
        byte[] key = RandomUtil.randomBytes(16);
        return Base64.getEncoder().encodeToString(key);
    }
    
    /**
     * SM4加密(ECB模式)
     * @param content 待加密内容
     * @param key Base64编码的密钥
     * @return Base64编码的密文
     */
    public static String encryptECB(String content, String key) {
        try {
            byte[] keyBytes = Base64.getDecoder().decode(key);
            SM4 sm4 = SmUtil.sm4(keyBytes);
            byte[] encrypted = sm4.encrypt(content.getBytes(StandardCharsets.UTF_8));
            return Base64.getEncoder().encodeToString(encrypted);
        } catch (Exception e) {
            log.error("SM4加密失败", e);
            throw new RuntimeException("SM4加密失败", e);
        }
    }
    
    /**
     * SM4解密(ECB模式)
     * @param encryptedContent Base64编码的密文
     * @param key Base64编码的密钥
     * @return 明文
     */
    public static String decryptECB(String encryptedContent, String key) {
        try {
            byte[] keyBytes = Base64.getDecoder().decode(key);
            SM4 sm4 = SmUtil.sm4(keyBytes);
            byte[] encrypted = Base64.getDecoder().decode(encryptedContent);
            byte[] decrypted = sm4.decrypt(encrypted);
            return new String(decrypted, StandardCharsets.UTF_8);
        } catch (Exception e) {
            log.error("SM4解密失败", e);
            throw new RuntimeException("SM4解密失败", e);
        }
    }
    
//    /**
//     * SM4加密(CBC模式，更安全)
//     * @param content 待加密内容
//     * @param key Base64编码的密钥
//     * @param iv Base64编码的初始向量(16字节)
//     * @return Base64编码的密文
//     */
//    public static String encryptCBC(String content, String key, String iv) {
//        try {
//            byte[] keyBytes = Base64.getDecoder().decode(key);
//            byte[] ivBytes = Base64.getDecoder().decode(iv);
//            SM4 sm4 = SmUtil.sm4(keyBytes);
//            byte[] encrypted = sm4.encryptCbc(content.getBytes(StandardCharsets.UTF_8), ivBytes);
//            return Base64.getEncoder().encodeToString(encrypted);
//        } catch (Exception e) {
//            log.error("SM4 CBC模式加密失败", e);
//            throw new RuntimeException("SM4 CBC模式加密失败", e);
//        }
//    }
//
//    /**
//     * SM4解密(CBC模式)
//     * @param encryptedContent Base64编码的密文
//     * @param key Base64编码的密钥
//     * @param iv Base64编码的初始向量(16字节)
//     * @return 明文
//     */
//    public static String decryptCBC(String encryptedContent, String key, String iv) {
//        try {
//            byte[] keyBytes = Base64.getDecoder().decode(key);
//            byte[] ivBytes = Base64.getDecoder().decode(iv);
//            SM4 sm4 = SmUtil.sm4(keyBytes);
//            byte[] encrypted = Base64.getDecoder().decode(encryptedContent);
//            byte[] decrypted = sm4.decryptCbc(encrypted, ivBytes);
//            return new String(decrypted, StandardCharsets.UTF_8);
//        } catch (Exception e) {
//            log.error("SM4 CBC模式解密失败", e);
//            throw new RuntimeException("SM4 CBC模式解密失败", e);
//        }
//    }
    
    /**
     * 生成随机初始向量
     * @return Base64编码的16字节随机初始向量
     */
    public static String generateIV() {
        // SM4的初始向量长度为128位，即16字节
        byte[] iv = RandomUtil.randomBytes(16);
        return Base64.getEncoder().encodeToString(iv);
    }
}