package com.kite.im.utils;

import com.google.zxing.*;
import com.google.zxing.Reader;
import com.google.zxing.client.j2se.BufferedImageLuminanceSource;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.common.HybridBinarizer;
import com.google.zxing.qrcode.QRCodeReader;
import com.google.zxing.qrcode.QRCodeWriter;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.Base64;

public class QrCodeUtils {

    /**
     * 生成带有对象数据的二维码图片
     *
     * @param data 对象数据
     * @return Base64编码的二维码图片字符串
     */
    public static byte[] generateQrCode(Object data) {
        try {
            // 创建二维码写入器
            QRCodeWriter qrCodeWriter = new QRCodeWriter();
            BitMatrix bitMatrix = qrCodeWriter.encode(String.valueOf(data), BarcodeFormat.QR_CODE, 200, 200);

            // 创建 BufferedImage 对象来绘制二维码图像
            BufferedImage qrCodeImage = new BufferedImage(200, 200, BufferedImage.TYPE_INT_RGB);
            qrCodeImage.createGraphics();

            // 将 BitMatrix 转换为 BufferedImage
            Graphics2D graphics = (Graphics2D) qrCodeImage.getGraphics();
            graphics.setColor(Color.WHITE);
            graphics.fillRect(0, 0, 200, 200);
            graphics.setColor(Color.BLACK);

            for (int i = 0; i < 200; i++) {
                for (int j = 0; j < 200; j++) {
                    if (bitMatrix.get(i, j)) {
                        graphics.fillRect(i, j, 1, 1);
                    }
                }
            }

            // 将 BufferedImage 转换为字节数组
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ImageIO.write(qrCodeImage, "png", baos);
            return baos.toByteArray();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

}
