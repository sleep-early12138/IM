package com.kite.im.utils;

import cn.hutool.json.JSONObject;

/**
 * JSON工具类，基于Hutool的JSONUtil封装，用于替代fastjson
 */
public class JSONUtil {

    /**
     * 将对象转换为JSON字符串
     *
     * @param object 要转换的对象
     * @return JSON字符串
     */
    public static String toJSONString(Object object) {
        return cn.hutool.json.JSONUtil.toJsonStr(object);
    }

    /**
     * 将JSON字符串解析为指定类型的对象
     *
     * @param text  JSON字符串
     * @param clazz 目标类型
     * @param <T>   泛型
     * @return 解析后的对象
     */
    public static <T> T parseObject(String text, Class<T> clazz) {
        return cn.hutool.json.JSONUtil.toBean(text, clazz);
    }

    /**
     * 将JSON字符串解析为JSONObject
     *
     * @param text JSON字符串
     * @return JSONObject对象
     */
    public static JSONObject parseObject(String text) {
        return cn.hutool.json.JSONUtil.parseObj(text);
    }
} 