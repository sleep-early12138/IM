package com.kite.im.constant;

public interface ContactConstant {
    /**
     * 发起申请待审核
     */
    int FRIEND_PENDING = 0;
    /**
     * 同意好友申请
     */
    int FRIEND_AGREE = 1;

    /**
     * 拒绝好友申请
     */
    int FRIEND_REFUSE = 2;

    /**
     * 正常
     */

    int FRIEND_SHIP_NORMAL = 1;


    /**
     * 黑名单
     */
    int FRIEND_SHIP_BLACK = 2 ;

    /**
     * 已删除
     */
    int FRIEND_SHIP_DELETE = 3 ;
}
