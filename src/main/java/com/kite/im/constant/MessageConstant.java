package com.kite.im.constant;

public class MessageConstant {
    // 消息类型
    public static final int CONTENT_TYPE_TEXT = 0;    // 文本
    public static final int CONTENT_TYPE_IMAGE = 1;   // 图片
    public static final int CONTENT_TYPE_VOICE = 2;   // 语音
    public static final int CONTENT_TYPE_VIDEO = 3;   // 视频
    public static final int CONTENT_TYPE_FILE = 4;    // 文件
    public static final int CONTENT_TYPE_LOCATION = 5;// 位置
    public static final int CONTENT_TYPE_EMOJI = 6;   // 表情
    
    // 会话类型
    public static final int CONVERSATION_TYPE_SINGLE = 0;  // 单聊
    public static final int CONVERSATION_TYPE_GROUP = 1;   // 群聊
    
    // 消息状态
    public static final int MESSAGE_STATUS_UNREAD = 0;    // 未读
    public static final int MESSAGE_STATUS_READ = 1;      // 已读
    public static final int MESSAGE_STATUS_RECALL = 2;    // 撤回
    public static final int MESSAGE_STATUS_DELETE = 3;    // 删除
    
    // 会话状态
    public static final int CONVERSATION_STATUS_NORMAL = 0;    // 正常
    public static final int CONVERSATION_STATUS_TOP = 1;       // 置顶
    public static final int CONVERSATION_STATUS_MUTE = 2;      // 免打扰
    public static final int CONVERSATION_STATUS_HIDE = 3;      // 隐藏
} 