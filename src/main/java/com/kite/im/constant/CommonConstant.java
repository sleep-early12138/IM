package com.kite.im.constant;

/**
 * 通用常量
 */
public interface CommonConstant {

    /**
     * 升序
     */
    String SORT_ORDER_ASC = "ascend";

    /**
     * 降序
     */
    String SORT_ORDER_DESC = " descend";

    String PENDING = "0";
    String APPROVED = "1";
    String REJECTED = "2";
}
