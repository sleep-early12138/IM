package com.kite.im.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.kite.im.mapper.CertificateMapper;
import com.kite.im.model.entity.Certificate;
import com.kite.im.service.CertificateService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.security.*;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Base64;
import java.util.UUID;

/**
 * 数字证书服务实现类
 */
@Service
@Slf4j
public class CertificateServiceImpl extends ServiceImpl<CertificateMapper, Certificate> implements CertificateService {

    // CA根证书的用户ID标识
    private static final Long CA_ROOT_ID = 0L;
    // 证书默认有效期(1年)
    private static final long CERTIFICATE_VALIDITY_PERIOD = 365 * 24 * 60 * 60 * 1000L;
    // 密钥算法
    private static final String KEY_ALGORITHM = "RSA";
    // 签名算法
    private static final String SIGNATURE_ALGORITHM = "SHA256withRSA";
    // 密钥长度
    private static final int KEY_SIZE = 2048;
    
    @Override
    public Certificate generateCARootCertificate() {
        try {
            // 检查CA根证书是否已存在
            Certificate existingCA = getCertificateByUserId(CA_ROOT_ID);
            if (existingCA != null) {
                return existingCA;
            }
            
            // 生成RSA密钥对
            KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance(KEY_ALGORITHM);
            keyPairGenerator.initialize(KEY_SIZE);
            KeyPair keyPair = keyPairGenerator.generateKeyPair();
            
            // 获取公钥和私钥
            PublicKey publicKey = keyPair.getPublic();
            PrivateKey privateKey = keyPair.getPrivate();
            
            // 编码密钥为Base64字符串
            String publicKeyStr = Base64.getEncoder().encodeToString(publicKey.getEncoded());
            String privateKeyStr = Base64.getEncoder().encodeToString(privateKey.getEncoded());
            
            // 创建CA根证书
            Certificate caCertificate = new Certificate();
            caCertificate.setUserId(CA_ROOT_ID);
            caCertificate.setUserName("CA Root");
            caCertificate.setSerialNumber(generateSerialNumber());
            caCertificate.setPublicKey(publicKeyStr);
            caCertificate.setPrivateKey(privateKeyStr); // CA根证书存储私钥
            
            // CA根证书自签名
            String signature = signData(caCertificate.getSerialNumber() + caCertificate.getUserId() 
                    + caCertificate.getUserName() + caCertificate.getPublicKey(), privateKey);
            caCertificate.setSignature(signature);
            
            // 设置证书有效期
            long currentTime = System.currentTimeMillis();
            caCertificate.setValidFrom(currentTime);
            caCertificate.setValidTo(currentTime + CERTIFICATE_VALIDITY_PERIOD);
            caCertificate.setStatus(1); // 有效状态
            caCertificate.setCreateTime(currentTime);
            caCertificate.setUpdateTime(currentTime);
            
            // 保存CA根证书
            save(caCertificate);
            
            return caCertificate;
        } catch (Exception e) {
            log.error("生成CA根证书失败", e);
            throw new RuntimeException("生成CA根证书失败", e);
        }
    }
    
    @Override
    public Certificate generateUserCertificate(Long userId, String userName, String publicKey) {
        try {
            // 检查用户证书是否已存在
            Certificate existingCert = getCertificateByUserId(userId);
            if (existingCert != null) {
                return existingCert;
            }
            
            // 获取CA根证书
            Certificate caCertificate = getCertificateByUserId(CA_ROOT_ID);
            if (caCertificate == null) {
                caCertificate = generateCARootCertificate();
            }
            
            // 解码CA私钥
            PrivateKey caPrivateKey = decodePrivateKey(caCertificate.getPrivateKey());
            
            // 创建用户证书
            Certificate userCertificate = new Certificate();
            userCertificate.setUserId(userId);
            userCertificate.setUserName(userName);
            userCertificate.setSerialNumber(generateSerialNumber());
            userCertificate.setPublicKey(publicKey);
            userCertificate.setPrivateKey(null); // 用户证书不存储私钥
            
            // CA对用户证书签名
            String signature = signData(userCertificate.getSerialNumber() + userCertificate.getUserId() 
                    + userCertificate.getUserName() + userCertificate.getPublicKey(), caPrivateKey);
            userCertificate.setSignature(signature);
            
            // 设置证书有效期
            long currentTime = System.currentTimeMillis();
            userCertificate.setValidFrom(currentTime);
            userCertificate.setValidTo(currentTime + CERTIFICATE_VALIDITY_PERIOD);
            userCertificate.setStatus(1); // 有效状态
            userCertificate.setCreateTime(currentTime);
            userCertificate.setUpdateTime(currentTime);
            
            // 保存用户证书
            save(userCertificate);
            
            return userCertificate;
        } catch (Exception e) {
            log.error("生成用户证书失败: userId={}", userId, e);
            throw new RuntimeException("生成用户证书失败", e);
        }
    }
    
    @Override
    public boolean verifyCertificate(Certificate certificate) {
        try {
            // 检查证书状态
            if (certificate.getStatus() != 1) {
                return false;
            }
            
            // 检查证书有效期
            long currentTime = System.currentTimeMillis();
            if (currentTime < certificate.getValidFrom() || currentTime > certificate.getValidTo()) {
                return false;
            }
            
            // 获取CA根证书
            Certificate caCertificate = getCertificateByUserId(CA_ROOT_ID);
            if (caCertificate == null) {
                return false;
            }
            
            // 解码CA公钥
            PublicKey caPublicKey = decodePublicKey(caCertificate.getPublicKey());
            
            // 验证证书签名
            String data = certificate.getSerialNumber() + certificate.getUserId() 
                    + certificate.getUserName() + certificate.getPublicKey();
            return verifySignature(data, certificate.getSignature(), caPublicKey);
        } catch (Exception e) {
            log.error("验证证书失败: serialNumber={}", certificate.getSerialNumber(), e);
            return false;
        }
    }
    
    @Override
    public Certificate getCertificateByUserId(Long userId) {
        QueryWrapper<Certificate> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("user_id", userId);
        return getOne(queryWrapper);
    }
    
    @Override
    public Certificate getCertificateBySerialNumber(String serialNumber) {
        QueryWrapper<Certificate> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("user_id", serialNumber);
        return getOne(queryWrapper);
    }
    
    @Override
    public boolean revokeCertificate(String serialNumber) {
        Certificate certificate = getCertificateBySerialNumber(serialNumber);
        if (certificate == null) {
            return false;
        }
        
        // 吊销证书
        certificate.setStatus(0);
        certificate.setUpdateTime(System.currentTimeMillis());
        return updateById(certificate);
    }
    
    /**
     * 生成唯一的证书序列号
     */
    private String generateSerialNumber() {
        return UUID.randomUUID().toString().replace("-", "").toUpperCase();
    }
    
    /**
     * 使用私钥对数据进行签名
     */
    private String signData(String data, PrivateKey privateKey) throws Exception {
        Signature signature = Signature.getInstance(SIGNATURE_ALGORITHM);
        signature.initSign(privateKey);
        signature.update(data.getBytes());
        return Base64.getEncoder().encodeToString(signature.sign());
    }
    
    /**
     * 使用公钥验证签名
     */
    private boolean verifySignature(String data, String signatureStr, PublicKey publicKey) throws Exception {
        Signature signature = Signature.getInstance(SIGNATURE_ALGORITHM);
        signature.initVerify(publicKey);
        signature.update(data.getBytes());
        return signature.verify(Base64.getDecoder().decode(signatureStr));
    }
    
    /**
     * 解码Base64编码的私钥
     */
    private PrivateKey decodePrivateKey(String privateKeyStr) throws Exception {
        byte[] keyBytes = Base64.getDecoder().decode(privateKeyStr);
        PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(keyBytes);
        KeyFactory keyFactory = KeyFactory.getInstance(KEY_ALGORITHM);
        return keyFactory.generatePrivate(keySpec);
    }
    
    /**
     * 解码Base64编码的公钥
     */
    private PublicKey decodePublicKey(String publicKeyStr) throws Exception {
        byte[] keyBytes = Base64.getDecoder().decode(publicKeyStr);
        X509EncodedKeySpec keySpec = new X509EncodedKeySpec(keyBytes);
        KeyFactory keyFactory = KeyFactory.getInstance(KEY_ALGORITHM);
        return keyFactory.generatePublic(keySpec);
    }
}