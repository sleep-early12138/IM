package com.kite.im.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.kite.im.mapper.QunMapper;
import com.kite.im.mapper.UserMapper;
import com.kite.im.model.entity.Qun;
import com.kite.im.model.entity.QunMember;
import com.kite.im.model.entity.User;
import com.kite.im.model.param.qun.RemoveMemberParam;
import com.kite.im.model.vo.QunMemberVO;
import com.kite.im.service.QunMemberService;
import com.kite.im.mapper.QunMemberMapper;
import com.kite.im.service.UserService;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

/**
 * @author lizhouwei
 * @description 针对表【qun_member(qun_member)】的数据库操作Service实现
 * @createDate 2023-06-20 20:26:25
 */
@Service
public class QunMemberServiceImpl extends ServiceImpl<QunMemberMapper, QunMember> implements QunMemberService {

    @Resource
    private QunMemberMapper qunMemberMapper;

    @Resource
    private QunMapper qunMapper;

    @Resource
    private UserService userService;

    @Resource
    private UserMapper userMapper;

    /**
     * 退出群聊
     * @param qunId
     * @return
     */
    @Override
    public Object quitQun(Long qunId, HttpServletRequest request) {
        // 1. 验证输入参数
        Assert.notNull(qunId, "群聊ID为空！");

        // 2. 查询群聊信息
        Qun qun = qunMapper.queryQunDetail(qunId);
        Assert.notNull(qun, "群聊不存在！");

        // 3. 查询用户信息
        Long userId = userService.getLoginUser(request).getId();

        // 4. 查询群里是否有该用户
        QunMember qunMember = qunMemberMapper.queryQunMemberByQunIdAndUserId(qunId, userId);
        Assert.notNull(qunMember, "用户不在群里！");

        // 5. 退出群聊
        qunMemberMapper.deleteMember(qunId, userId);

        // 6. TODO 通知群里其他成员
        return "退出群聊成功！";
    }

    /**
     * 踢出群聊
     * @param removeMemberParam
     * @return
     */
    @Override
    public Object kickOutQun(RemoveMemberParam removeMemberParam) {
        // 1. 验证输入参数
        Assert.notNull(removeMemberParam.getQunId(), "群聊ID为空！");
        Assert.notNull(removeMemberParam.getMemberId(), "用户ID为空！");
        Long userId = userService.getLoginUser(removeMemberParam.getRequest()).getId();
        Assert.isTrue(userId.equals(removeMemberParam.getMemberId()), "只有群主才能踢出群成员！");

        // 2. 查询群聊信息
        Qun qun = qunMapper.queryQunDetail( removeMemberParam.getQunId());
        Assert.notNull(qun, "群聊不存在！");
        // 4. 查询群里是否有该用户
        QunMember qunMember = qunMemberMapper.queryQunMemberByQunIdAndUserId( removeMemberParam.getQunId(), userId);
        Assert.notNull(qunMember, "用户不在群里！");
        // 5. 踢出群聊
        qunMemberMapper.deleteMember(removeMemberParam.getQunId(), userId);
        // 6. 查询群里是否有该用户
        QunMember removeQunMember = qunMemberMapper.queryQunMemberByQunIdAndUserId(removeMemberParam.getQunId(), userId);
        Assert.isNull(removeQunMember, "踢出群聊失败！");
        // 7. TODO 通知群里其他成员
        return "踢出群聊成功！";
    }

    /**
     * 转让群主
     * @param qunId
     * @param newOwner
     * @return
     */
    @Override
    public Object transferQun(Long qunId, Long newOwner) {
        int flag = qunMapper.updateOwn(qunId, newOwner);
        Assert.isTrue(flag > 0, "转让群主失败！");
        // 6. TODO 通知群里其他成员
        return "转让群主成功！";
    }

    /**
     * 查询群成员列表
     * @param qunId
     * @return
     */
    @Override
    public List<QunMemberVO> queryQunMemberList(Long qunId) {
        List<QunMember> members = qunMemberMapper.queryQunMemberList(qunId);
        List<QunMemberVO> qunMemberVOS = new ArrayList<>();
        for (QunMember qunMember : members) {
            QunMemberVO qunMemberVO = new QunMemberVO();
            qunMemberVO.setId(Long.valueOf(qunMember.getMemberId()));
            User user = userMapper.queryById(qunMember.getMemberId());
            qunMemberVO.setUserName(user.getUserName());
            qunMemberVO.setUserAvatar(user.getUserAvatar());
            qunMemberVOS.add(qunMemberVO);
        }
        Assert.notNull(qunMemberVOS, "空空如也，快去邀请人加入吧！");
        return qunMemberVOS;
    }
}




