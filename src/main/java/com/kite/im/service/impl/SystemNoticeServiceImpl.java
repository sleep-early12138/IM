package com.kite.im.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.kite.im.mapper.SystemNoticeMapper;
import com.kite.im.model.entity.SystemNotice;
import com.kite.im.service.SystemNoticeService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 系统通知服务实现类
 */
@Service
public class SystemNoticeServiceImpl extends ServiceImpl<SystemNoticeMapper, SystemNotice> implements SystemNoticeService {

    @Override
    public boolean createNotice(SystemNotice systemNotice) {
        return save(systemNotice);
    }

    @Override
    public boolean updateNotice(SystemNotice systemNotice) {
        return updateById(systemNotice);
    }

    @Override
    public boolean deleteNotice(Long noticeId) {
        // 逻辑删除
        SystemNotice notice = new SystemNotice();
        notice.setNoticeId(noticeId);
        notice.setIsDelete(1);
        return updateById(notice);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean batchDeleteNotices(List<Long> noticeIds) {
        // 批量逻辑删除
        return update(new SystemNotice().setIsDelete(1),
                new LambdaQueryWrapper<SystemNotice>()
                        .in(SystemNotice::getNoticeId, noticeIds));
    }

    @Override
    public SystemNotice getNoticeById(Long noticeId) {
        return getOne(new LambdaQueryWrapper<SystemNotice>()
                .eq(SystemNotice::getNoticeId, noticeId)
                .eq(SystemNotice::getIsDelete, 0));
    }

    @Override
    public IPage<SystemNotice> getUserNotices(Long receiverId, long page, long size) {
        return page(new Page<>(page, size),
                new LambdaQueryWrapper<SystemNotice>()
                        .eq(SystemNotice::getReceiverId, receiverId)
                        .eq(SystemNotice::getIsDelete, 0)
                        .orderByDesc(SystemNotice::getCreateTime));
    }

    @Override
    public boolean markAsRead(Long noticeId) {
        SystemNotice notice = new SystemNotice();
        notice.setNoticeId(noticeId);
        notice.setReadStatus(1);
        return updateById(notice);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean batchMarkAsRead(List<Long> noticeIds) {
        return update(new SystemNotice().setReadStatus(1),
                new LambdaQueryWrapper<SystemNotice>()
                        .in(SystemNotice::getNoticeId, noticeIds));
    }

    @Override
    public long getUnreadCount(Long receiverId) {
        return count(new LambdaQueryWrapper<SystemNotice>()
                .eq(SystemNotice::getReceiverId, receiverId)
                .eq(SystemNotice::getReadStatus, 0)
                .eq(SystemNotice::getIsDelete, 0));
    }
}