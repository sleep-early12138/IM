package com.kite.im.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import javax.servlet.http.HttpServletRequest;

import com.kite.im.model.entity.Certificate;
import com.kite.im.service.CertificateService;

import cn.dev33.satoken.stp.StpUtil;
import com.kite.im.common.ErrorCode;
import com.kite.im.constant.CommonConstant;
import com.kite.im.exception.BusinessException;
import com.kite.im.mapper.UserMapper;
import com.kite.im.model.param.user.UserQueryRequest;
import com.kite.im.model.entity.User;
import com.kite.im.model.enums.UserRoleEnum;
import com.kite.im.model.vo.LoginUserVO;
import com.kite.im.model.vo.UserVO;
import com.kite.im.service.UserService;
import com.kite.im.utils.SqlUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.util.DigestUtils;


/**
 * 用户服务实现
 *
 * @author lizhouwei
 * @description 针对表【chart(用户表)】的数据库操作Service实现
 * @createDate 2023-05-19 16:20:08
 */
@Service
@Slf4j
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {

    /**
     * 盐值，混淆密码
     */
    private static final String SALT = "kite";
    
    /**
     * 密钥算法
     */
    private static final String KEY_ALGORITHM = "RSA";
    
    /**
     * 密钥长度
     */
    private static final int KEY_SIZE = 2048;
    
    @javax.annotation.Resource
    private CertificateService certificateService;

    @Override
    @Transactional
    public long userRegister(String userAccount, String userPassword, String checkPassword, String userEmail) {
        // 1. 校验
        if (StringUtils.isAnyBlank(userAccount, userPassword, checkPassword)) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR, "参数为空");
        }
        if (userAccount.length() < 4) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR, "用户账号过短");
        }
        if (userPassword.length() < 8 || checkPassword.length() < 8) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR, "用户密码过短");
        }
        // 密码和校验密码相同user
        if (!userPassword.equals(checkPassword)) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR, "两次输入的密码不一致");
        }
        synchronized (userAccount.intern()) {
            // 账户不能重复
            QueryWrapper<User> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("userAccount", userAccount);
            long count = this.baseMapper.selectCount(queryWrapper);
            if (count > 0) {
                throw new BusinessException(ErrorCode.PARAMS_ERROR, "账号重复");
            }
            // 2. 加密
            String encryptPassword = DigestUtils.md5DigestAsHex((SALT + userPassword).getBytes());
            // 3. 生成RSA密钥对
            String publicKeyStr = null;
            String privateKeyStr = null;
            try {
                java.security.KeyPairGenerator keyPairGenerator = java.security.KeyPairGenerator.getInstance(KEY_ALGORITHM);
                keyPairGenerator.initialize(KEY_SIZE);
                java.security.KeyPair keyPair = keyPairGenerator.generateKeyPair();
                
                // 获取公钥和私钥
                java.security.PublicKey publicKey = keyPair.getPublic();
                java.security.PrivateKey privateKey = keyPair.getPrivate();
                
                // 编码密钥为Base64字符串
                publicKeyStr = java.util.Base64.getEncoder().encodeToString(publicKey.getEncoded());
                privateKeyStr = java.util.Base64.getEncoder().encodeToString(privateKey.getEncoded());
            } catch (Exception e) {
                log.error("生成密钥对失败", e);
                throw new BusinessException(ErrorCode.SYSTEM_ERROR, "生成密钥对失败");
            }
            
            // 4. 插入数据
            User user = new User();
            user.setUserAccount(userAccount);
            user.setUserPassword(encryptPassword);
            // 设置默认角色
            user.setUserRole(UserRoleEnum.USER.getValue());
            // 设置密钥对
            user.setPublicKey(publicKeyStr);
            user.setPrivateKey(privateKeyStr);
            user.setUserEmail(userEmail);
            // 设置数字证书默认值，避免数据库插入错误
            user.setDigitalCertificate("pending");
            boolean saveResult = this.save(user);
            if (!saveResult) {
                throw new BusinessException(ErrorCode.SYSTEM_ERROR, "注册失败，数据库错误");
            }
            
            // 5. 生成数字证书
            try {
                // 用户ID需要转为Long类型，因为CertificateService接口要求Long类型的userId
                Long userId = Long.valueOf(user.getId());
                String userName = StringUtils.isNotBlank(user.getUserName()) ? user.getUserName() : user.getUserAccount();
                Certificate certificate = certificateService.generateUserCertificate(userId, userName, publicKeyStr);
                
                // 更新用户的数字证书信息
                if (certificate != null) {
                    user.setDigitalCertificate(certificate.getSerialNumber());
                    this.updateById(user);
                }
            } catch (Exception e) {
                log.error("生成数字证书失败: userId={}", user.getId(), e);
                // 证书生成失败不影响用户注册，只记录日志
            }
            
            return user.getId();
        }
    }

    @Override
    public LoginUserVO userLogin(String userAccount, String userPassword, HttpServletRequest request) {
        // 1. 校验
        Assert.isTrue(StringUtils.isNoneBlank(userAccount, userPassword), "参数为空");
        Assert.isTrue(userAccount.length() >= 4, "用户账号过短");
        Assert.isTrue(userPassword.length() >= 8, "用户密码过短");

        // 2. 加密
        String encryptPassword = DigestUtils.md5DigestAsHex((SALT + userPassword).getBytes());
        // 查询用户是否存在
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("userAccount", userAccount);
        queryWrapper.eq("userPassword", encryptPassword);
        User user = this.baseMapper.selectOne(queryWrapper);
        // 用户不存在
        Assert.isTrue(user != null, "用户不存在或密码错误");
        // 3. 使用Sa-Token记录用户登录状态
        StpUtil.login(user.getId());
        return this.getLoginUserVO(user);
    }


    /**
     * 获取当前登录用户
     *
     * @param request
     * @return
     */
    @Override
    public User getLoginUser(HttpServletRequest request) {
        // 获取当前登录用户id
        long loginId = StpUtil.getLoginIdAsLong();
        // 从数据库查询用户信息
        User currentUser = this.getById(loginId);
        if (currentUser == null) {
            throw new BusinessException(ErrorCode.NOT_LOGIN_ERROR);
        }
        return currentUser;
    }

    /**
     * 获取当前登录用户（允许未登录）
     *
     * @param request
     * @return
     */
    @Override
    public User getLoginUserPermitNull(HttpServletRequest request) {
        // 判断是否已登录
        if (!StpUtil.isLogin()) {
            return null;
        }
        // 获取当前登录用户id
        long loginId = StpUtil.getLoginIdAsLong();
        // 从数据库查询用户信息
        return this.getById(loginId);
    }

    /**
     * 是否为管理员
     *
     * @param request
     * @return
     */
    @Override
    public boolean isAdmin(HttpServletRequest request) {
        // 判断是否已登录
        if (!StpUtil.isLogin()) {
            return false;
        }
        // 获取当前登录用户
        User user = getLoginUser(request);
        return isAdmin(user);
    }

    @Override
    public boolean isAdmin(User user) {
        return user != null && UserRoleEnum.ADMIN.getValue().equals(user.getUserRole());
    }

    /**
     * 用户注销
     *
     * @param request
     */
    @Override
    public boolean userLogout(HttpServletRequest request) {
        if (!StpUtil.isLogin()) {
            throw new BusinessException(ErrorCode.NOT_LOGIN_ERROR, "未登录");
        }
        // 移除登录状态
        StpUtil.logout();
        return true;
    }

    @Override
    public LoginUserVO getLoginUserVO(User user) {
        if (user == null) {
            return null;
        }
        LoginUserVO loginUserVO = new LoginUserVO();
        BeanUtils.copyProperties(user, loginUserVO);
        loginUserVO.setToken(StpUtil.getTokenValue());
        loginUserVO.setId(user.getId());
        return loginUserVO;
    }

    @Override
    public UserVO getUserVO(User user) {
        if (user == null) {
            return null;
        }
        UserVO userVO = new UserVO();
        BeanUtils.copyProperties(user, userVO);
        return userVO;
    }

    @Override
    public List<UserVO> getUserVO(List<User> userList) {
        if (CollectionUtils.isEmpty(userList)) {
            return new ArrayList<>();
        }
        return userList.stream().map(this::getUserVO).collect(Collectors.toList());
    }

    @Override
    public QueryWrapper<User> getQueryWrapper(UserQueryRequest userQueryRequest) {
        if (userQueryRequest == null) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR, "请求参数为空");
        }
        Long id = userQueryRequest.getId();
        String unionId = userQueryRequest.getUnionId();
        String mpOpenId = userQueryRequest.getMpOpenId();
        String userName = userQueryRequest.getUserName();
        String userProfile = userQueryRequest.getUserProfile();
        String userRole = userQueryRequest.getUserRole();
        String sortField = userQueryRequest.getSortField();
        String sortOrder = userQueryRequest.getSortOrder();
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(id != null, "id", id);
        queryWrapper.eq(StringUtils.isNotBlank(unionId), "unionId", unionId);
        queryWrapper.eq(StringUtils.isNotBlank(mpOpenId), "mpOpenId", mpOpenId);
        queryWrapper.eq(StringUtils.isNotBlank(userRole), "userRole", userRole);
        queryWrapper.like(StringUtils.isNotBlank(userProfile), "userProfile", userProfile);
        queryWrapper.like(StringUtils.isNotBlank(userName), "userName", userName);
        queryWrapper.orderBy(SqlUtils.validSortField(sortField), sortOrder.equals(CommonConstant.SORT_ORDER_ASC),
                sortField);
        return queryWrapper;
    }
}
