package com.kite.im.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.kite.im.mapper.QunMemberMapper;
import com.kite.im.mapper.UserMapper;
import com.kite.im.model.entity.*;
import com.kite.im.model.param.qun.InviteFriendParam;
import com.kite.im.model.param.qun.ModifyQunParam;
import com.kite.im.model.param.qun.QunCreateParam;
import com.kite.im.model.param.qun.ReplyInviteParam;
import com.kite.im.model.vo.QunVO;
import com.kite.im.service.ContactService;
import com.kite.im.service.QunService;
import com.kite.im.mapper.QunMapper;
import com.kite.im.service.UserService;
import com.kite.im.utils.QrCodeUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;


/**
 * @author lizhouwei
 * @description 针对表【qun(qun)】的数据库操作Service实现
 * @createDate 2023-06-20 20:26:25
 */
@Service
public class QunServiceImpl extends ServiceImpl<QunMapper, Qun> implements QunService {

    @Resource
    private QunMapper qunMapper;

    @Resource
    private UserService userService;

    @Resource
    private UserMapper userMapper;
    @Resource
    private ContactService contactService;

    @Resource
    private QunMemberMapper qunMemberMapper;



    /**
     * 创建群聊
     *
     * @param qunCreateParam
     * @return
     */
    @Override
    public Long newQun(QunCreateParam qunCreateParam) {
        Assert.notNull(qunCreateParam.getName(), "群聊名称不能为空");
        Assert.notNull(qunCreateParam.getCategoryId(), "群聊分类不能为空");
        Assert.notNull(qunCreateParam.getOwnerId(), "群聊创建者不能为空");
        Qun qun = new Qun();
        qun.setName(qunCreateParam.getName());
        qun.setCategoryId(qunCreateParam.getCategoryId());
        qun.setOwnerId(qunCreateParam.getOwnerId());
        qun.setAnnouncement(qunCreateParam.getAnnouncement());
        qun.setNationalityId(qunCreateParam.getNationalityId());
        qun.setRemark(qunCreateParam.getRemark());

        int flag = qunMapper.insert(qun);
        Assert.isTrue(flag > 0, "创建群失败");
        return qun.getId();
    }


    /**
     * 修改群聊信息
     *
     * @param modifyQunParam
     * @return
     */
    @Override
    public Long modifyQun(ModifyQunParam modifyQunParam) {
        Assert.notNull(modifyQunParam.getQunId(), "群聊id不能为空");
        Qun qun = new Qun();
        qun.setId(modifyQunParam.getQunId());
        qun.setName(modifyQunParam.getName());
        qun.setAnnouncement(modifyQunParam.getAnnouncement());
        qun.setRemark(modifyQunParam.getRemark());

        int flag = qunMapper.updateById(qun);
        Assert.isTrue(flag > 0, "创建群失败");
        return qun.getId();
    }

    /**
     * 查询群聊详情
     *
     * @param qunId
     * @return
     */
    @Override
    public QunVO queryQunDetail(Long qunId) {
        Qun qun = qunMapper.queryQunDetail(qunId);
        if (qun == null) {
            return null;
        }
        QunVO qunVO = new QunVO();
        qunVO.setQunId(qun.getId());
        qunVO.setName(qun.getName());
        qunVO.setAnnouncement(qun.getAnnouncement());
        qunVO.setNationality(qunVO.getNationality());
        qunVO.setRemark(qunVO.getRemark());
        qunVO.setOwnerId(qunVO.getOwnerId());
        qunVO.setOwnerName(qunVO.getOwnerName());
        qunVO.setCategoryId(qunVO.getCategoryId());
        qunVO.setCategoryName(qunVO.getCategoryName());
        return qunVO;
    }

    /**
     * 查询群聊列表
     *
     * @param categoryId
     * @return
     */
    @Override
    public List<QunVO> queryQunList(Long categoryId) {
        List<Qun> quns = qunMapper.queryQunList(categoryId);
        if (quns == null || quns.size() == 0) {
            return null;
        }
        List<QunVO> qunList = new ArrayList<>();
        for (Qun qun : quns) {
            QunVO qunVO = new QunVO();
            qunVO.setQunId(qun.getId());
            qunVO.setName(qun.getName());
            qunVO.setAnnouncement(qun.getAnnouncement());
            qunVO.setNationality(qunVO.getNationality());
            qunVO.setRemark(qunVO.getRemark());
            qunVO.setOwnerId(qunVO.getOwnerId());
            qunVO.setOwnerName(qunVO.getOwnerName());
            qunVO.setCategoryId(qunVO.getCategoryId());
            qunVO.setCategoryName(qunVO.getCategoryName());
            qunList.add(qunVO);
        }
        return qunList;
    }

    /**
     * 查询我创建的群聊列表
     *
     * @return
     */
    @Override
    public List<QunVO> queryMyCreateQunList(HttpServletRequest request) {
        Long userId = userService.getLoginUser(request).getId();
        List<Qun> quns = qunMapper.queryMyCreateQunList(userId);
        if (quns == null || quns.size() == 0) {
            return null;
        }
        List<QunVO> qunList = new ArrayList<>();
        for (Qun qun : quns) {
            QunVO qunVO = new QunVO();
            qunVO.setQunId(qun.getId());
            qunVO.setName(qun.getName());
            qunVO.setAnnouncement(qun.getAnnouncement());
            qunVO.setNationality(qunVO.getNationality());
            qunVO.setRemark(qunVO.getRemark());
            qunVO.setOwnerId(qunVO.getOwnerId());
            qunVO.setOwnerName(qunVO.getOwnerName());
            qunVO.setCategoryId(qunVO.getCategoryId());
            qunVO.setCategoryName(qunVO.getCategoryName());
            qunList.add(qunVO);
        }
        return qunList;
    }

    @Override
    public List<QunVO> queryMyJoinQunList(HttpServletRequest request) {
        Long userId = userService.getLoginUser(request).getId();
        List<Qun> quns = qunMapper.queryMyJoinQunList(userId);
        if (quns == null || quns.size() == 0) {
            return null;
        }
        List<QunVO> qunList = new ArrayList<>();
        for (Qun qun : quns) {
            QunVO qunVO = new QunVO();
            qunVO.setQunId(qun.getId());
            qunVO.setName(qun.getName());
            qunVO.setAnnouncement(qun.getAnnouncement());
            qunVO.setNationality(qunVO.getNationality());
            qunVO.setRemark(qunVO.getRemark());
            qunVO.setOwnerId(qunVO.getOwnerId());
            qunVO.setOwnerName(qunVO.getOwnerName());
            qunVO.setCategoryId(qunVO.getCategoryId());
            qunVO.setCategoryName(qunVO.getCategoryName());
            qunList.add(qunVO);
        }
        return qunList;
    }

    /**
     * 生成群聊二维码
     *
     * @param qunId
     * @return
     */
    @Override
    public byte[] generateQRCode(Long qunId) {
        return QrCodeUtils.generateQrCode(qunId);
    }

    /**
     * 邀请好友加入群聊
     *
     * @param inviteFriendParam
     * @return
     */
    @Override
    public Object inviteFriend(InviteFriendParam inviteFriendParam, HttpServletRequest request) {
        // 1. 验证输入参数
        Assert.notNull(inviteFriendParam, "邀请参数为空！");
        Assert.notNull(inviteFriendParam.getQunId(), "群聊ID为空！");
        Assert.notNull(inviteFriendParam.getFriendId(), "好友ID为空！");
        Long userId = userService.getLoginUser(request).getId();
        Assert.isTrue(!userId.equals(inviteFriendParam.getFriendId()), "不能自己添加自己！");

        // 2. 查询好友信息
        FriendShip friend = contactService.queryFriendById(userId, inviteFriendParam.getFriendId());
        Assert.isTrue(friend == null, "好友不存在！");
        // 3. 查询群聊信息
        Qun qun = qunMapper.queryQunDetail(inviteFriendParam.getQunId());
        Assert.notNull(qun, "群聊不存在！");
        // 4. 验证是否已经是群成员
        Long memberId = inviteFriendParam.getFriendId();
        QunMember qunMember = qunMemberMapper.queryQunMemberByQunIdAndUserId(inviteFriendParam.getQunId(), memberId);
        Assert.isNull(qunMember, "该好友已经是群成员！");
        // TODO 5. 发送邀请链接
//        sendInvitationNotification(friend);
        // 6. 返回结果
        return "邀请已发送";
    }

    /**
     * 通过二维码加入群聊
     *
     * @param base64code
     * @return
     */
    @Override
    public Long joinQunByQRCode(String base64code) {
        return null;
    }





    /**
     * 回复邀请加入群聊
     *
     * @param replyInviteParam
     * @param request
     * @return
     */
    @Override
    public Object replyInviteQun(ReplyInviteParam replyInviteParam, HttpServletRequest request) {
        // 1. 验证输入参数
        Assert.notNull(replyInviteParam, "回复邀请参数为空！");
        Assert.notNull(replyInviteParam.getQunId(), "群聊ID为空！");
        Assert.notNull(replyInviteParam.getStaus(), "回复状态为空！");

        Long memberId = userService.getLoginUser(request).getId();

        // 2. 查询群聊信息
        Qun qun = qunMapper.queryQunDetail(replyInviteParam.getQunId());
        Assert.notNull(qun, "群聊不存在！");

        // 3. 判断邀请状态
        if (replyInviteParam.getStaus() == 1) {
            // 接受邀请的逻辑
            // 添加用户到群组中
            int falg = qunMemberMapper.addMemberToQun(replyInviteParam.getQunId(), memberId);
            Assert.isTrue(falg == 1, "加入群组失败！");
            // TODO 4. 发送接受邀请通知
//        sendAcceptInviteNotification(qun);

            // 5. 返回结果
            return "已接受邀请，加入群组成功！";
        } else if (replyInviteParam.getStaus() == 0) {
            // 拒绝邀请的逻辑
            // TODO 4. 发送拒绝邀请通知
//        sendRejectInviteNotification(qun);

            // 5. 返回结果
            return "已拒绝邀请，未加入群组！";
        } else {
            return "回复邀请处理失败！";
        }
    }


    /**
     * 解散群聊
     *
     * @param qunId
     * @param request
     * @return
     */
    @Override
    public Object dismissQun(Long qunId,HttpServletRequest request) {
        // 1. 验证输入参数
        Assert.notNull(qunId, "群聊ID为空！");

        // 2. 查询群聊信息
        Qun qun = qunMapper.queryQunDetail(qunId);
        Assert.notNull(qun, "群聊不存在！");

        // 3. 查询用户信息
        Long userId = userService.getLoginUser(request).getId();

        // 4. 判断用户是否是群主
        if (!qun.getOwnerId().equals(userId)) {
            return "只有群主才能解散群聊！";
        }

        // 5. 删除群聊
        int flag = qunMapper.deleteQun(qunId);

        //删除群成员
        int flag2 = qunMemberMapper.deleteQunMember(qunId);
        Assert.isTrue(flag == 1, "解散群聊失败！");
        Assert.isTrue(flag2 == 1, "解散群聊失败！");
        // 6. TODO 发送解散群聊通知
        return "解散群聊成功！";
    }
}








