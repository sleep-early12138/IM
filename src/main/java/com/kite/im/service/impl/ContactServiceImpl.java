package com.kite.im.service.impl;

import com.kite.im.controller.NotificationPushController;
import com.kite.im.mapper.FriendRequestMapper;
import com.kite.im.mapper.FriendShipMapper;
import com.kite.im.mapper.UserMapper;
import com.kite.im.model.entity.*;
import com.kite.im.model.param.audit.FriendRequestParam;
import com.kite.im.model.param.audit.QueryFriendParam;
import com.kite.im.model.query.ContactQuery;
import com.kite.im.service.CertificateService;
import com.kite.im.service.ContactService;
import com.kite.im.service.SystemNoticeService;
import com.kite.im.service.UserService;
import com.kite.im.utils.AESUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static com.kite.im.constant.ContactConstant.*;
import static com.kite.im.utils.AESUtil.*;

/**
 * @author: lizhouwei
 * @ date : 2023/5/29
 * @description:
 */
@Slf4j
@Service
public class ContactServiceImpl implements ContactService {

    @Resource
    private UserMapper userMapper;
    @Resource
    private FriendRequestMapper friendRequestMapper;
    @Resource
    private FriendShipMapper friendShipMapper;
    @Resource
    private UserService userService;
    @Resource
    private CertificateService  certificateService;
    @Resource
    private SystemNoticeService systemNoticeService;
    @Resource
    private NotificationPushController notificationPushController;


    /**
     * 根据邮箱查询用户信息
     *
     * @param userAccount
     * @return
     */
    @Override
    public QueryFriendParam queryByUserAccount(String userAccount, HttpServletRequest request) {
        Assert.hasText(userAccount, "邮箱不能为空");
        userService.getLoginUser(request);
        User user = userMapper.queryByUserAccount(userAccount);
        Assert.notNull(user, "用户不存在");
        String friendSecretIdentify = AESUtil.encrypt(KEY, IV, user.getId().toString());

        QueryFriendParam friendParam = new QueryFriendParam();
        friendParam.setFriendSecretIdentify(friendSecretIdentify);
        friendParam.setUserAvatar(user.getUserAvatar());
        friendParam.setUserName(user.getUserName());
        friendParam.setFriendSecretIdentify(friendSecretIdentify);
        return friendParam;

    }

    /**
     * 申请添加好友
     *
     * @param userId 返回给用户的证书序列号
     * @param reason
     * @return
     */
    @Override
    @Transactional
    public Long requestFriend(String userId, String reason, HttpServletRequest request) {
        Assert.hasText(userId, "序列号不能为空");
        FriendRequestParam friendRequest = new FriendRequestParam();
        //根据公钥查询证书信息
        Certificate certificateBySerialNumber = certificateService.getCertificateBySerialNumber(userId);
        Assert.notNull(certificateBySerialNumber, "证书不存在");
        //验证证书有效性
        Assert.isTrue(certificateService.verifyCertificate(certificateBySerialNumber), "证书无效或过期");

        //requesterId为被申请人Id
        Long receiverId = certificateBySerialNumber.getUserId();


        Long ownId = userService.getLoginUser(request).getId();
        Assert.isTrue(!receiverId.toString().equals(ownId.toString()), "不能添加自己为好友");
        
        // 检查是否已经是好友
        boolean isFriend = friendShipMapper.existFriend(ownId,receiverId);
        Assert.isTrue(!isFriend, "已经是好友了");
        
        // 检查是否已经发送过申请且未处理
        List<FriendRequest> existingRequests = friendRequestMapper.queryFriendRequestList(receiverId);
        if (existingRequests != null && !existingRequests.isEmpty()) {
            for (FriendRequest req : existingRequests) {
                // 状态为0表示待处理
                if (req.getStatus() != null && req.getStatus() == FRIEND_PENDING &&
                    req.getRequesterId().equals(ownId) && 
                    req.getReceiverId().equals(receiverId)) {
                    return req.getRequesterId(); // 已经发送过申请，直接返回
                }
            }
        }
        
        // 设置申请信息
        friendRequest.setRequesterId(ownId);
        friendRequest.setReason(reason);
        friendRequest.setReceiverId(receiverId);
        friendRequest.setStatus(FRIEND_PENDING); // 设置状态为待处理
        
        // 保存申请到数据库
        int i = friendRequestMapper.addFriend(friendRequest);
        Assert.isTrue(i > 0, "申请失败");
        

        // 可以通过WebSocket、消息队列或其他方式实现
        log.info("发送好友申请成功，申请人ID: {}，接收人ID: {}", ownId, receiverId);

        // 创建好友申请通知
        SystemNotice notice = new SystemNotice()
                .setSenderId(ownId)
                .setReceiverId(receiverId)
                .setNoticeType("friend_request")
                .setNoticeTitle("用户" + certificateBySerialNumber.getUserName() + "请求添加您为好友")
                .setNoticeContent(reason)
                .setSendTime(LocalDateTime.now())
                .setReadStatus(0)
                .setClickStatus(0);
        
        systemNoticeService.createNotice(notice);
        
        // 实时推送通知
        notificationPushController.pushNotification(receiverId, notice);
        
        return friendRequest.getRequesterId();
    }


    /**
     * 审核好友申请
     *
     * @param requestId
     * @param auditId
     * @param request
     * @return
     */
    public int requestFriendAudit(Long requestId, Long auditId, HttpServletRequest request) {
        Assert.notNull(requestId, "申请id不能为空");
        Assert.isTrue(friendRequestMapper.exist(requestId), "申请不存在");

        int status = auditId.intValue(); // 假设状态码不会超出 int 范围

        if (auditId == FRIEND_AGREE) {
            // 同意申请
            FriendRequest friendRequest = friendRequestMapper.querytById(requestId);
            Long userId = friendRequest.getReceiverId();
            Long friendId = friendRequest.getRequesterId();
            log.info("userId:{} friendId:{}", userId, friendId);

            // 成功，添加好友关系到好友关系表，将申请表中的状态改为已同意
            // 添加第一条好友关系记录
            FriendShip friendShip1 = new FriendShip();
            friendShip1.setUserId(userId);
            friendShip1.setFriendId(friendId);
            friendShip1.setStatus(FRIEND_SHIP_NORMAL);
            int result1 = friendShipMapper.addFriend(friendShip1);
            Assert.isTrue(result1 > 0, "添加好友失败");

            // 添加第二条好友关系记录
            FriendShip friendShip2 = new FriendShip();
            friendShip2.setUserId(friendId);
            friendShip2.setFriendId(userId);
            friendShip2.setStatus(FRIEND_SHIP_NORMAL);
            int result2 = friendShipMapper.addFriend(friendShip2);
            Assert.isTrue(result2 > 0, "添加好友失败");

            friendRequestMapper.updateStatus(requestId, status);

            // 创建好友申请通过通知
            FriendRequest requestAgree = friendRequestMapper.querytById(requestId);
            SystemNotice notice = new SystemNotice()
                    .setSenderId(userId)
                    .setReceiverId(friendId)
                    .setNoticeType("friend_request_accepted")
                    .setNoticeTitle("好友申请已通过")
                    .setNoticeContent("您的好友申请已被通过")
                    .setSendTime(LocalDateTime.now())
                    .setReadStatus(0)
                    .setClickStatus(0);
            
            systemNoticeService.createNotice(notice);
            
            // 实时推送通知
            notificationPushController.pushNotification(friendId, notice);
            
            return result1 + result2;
        }

        int result = friendRequestMapper.updateStatus(requestId, status);
        
        if (status == FRIEND_REFUSE) {
            // 创建好友申请拒绝通知
            FriendRequest requestReject = friendRequestMapper.querytById(requestId);
            SystemNotice notice = new SystemNotice()
                    .setSenderId(requestReject.getReceiverId())
                    .setReceiverId(requestReject.getRequesterId())
                    .setNoticeType("friend_request_rejected")
                    .setNoticeTitle("好友申请被拒绝")
                    .setNoticeContent("您的好友申请已被拒绝")
                    .setSendTime(LocalDateTime.now())
                    .setReadStatus(0)
                    .setClickStatus(0);
            
            systemNoticeService.createNotice(notice);
            
            // 实时推送通知
            notificationPushController.pushNotification(requestReject.getRequesterId(), notice);
        }
        
        return result;
    }


    /**
     * 查询好友联系人列表
     *
     * @return
     */
    @Override
    public List<ContactQuery> queryFriendList(HttpServletRequest request) {
        Long userId = userService.getLoginUser(request).getId();
        List<Long> friendId = friendShipMapper.queryFriendList(userId);
        if (CollectionUtils.isEmpty(friendId)) {
            return null;
        }
        List<ContactQuery> friendList = new ArrayList<>();
        for (Long id : friendId) {
            Assert.notNull(id, "好友id不能为空");
            friendList.add(userMapper.queryFriendList(id));
        }
        return friendList;
    }

    /**
     * 删除好友
     *
     * @param friendId
     * @param request
     * @return
     */
    @Override
    public int deleteFriend(Long friendId, HttpServletRequest request) {
        Assert.notNull(friendId, "自己id不能为空");
        Long userId = userService.getLoginUser(request).getId();
        int i = friendShipMapper.deleteFriend(userId, friendId, FRIEND_SHIP_DELETE);
        Assert.isTrue(i > 0, "删除好友失败");
        return i;
    }

    /**
     * 查询好友申请列表
     *
     * @param request
     * @return
     */
    @Override
    public List<FriendRequest> queryFriendRequestList(HttpServletRequest request) {
        Long userId = userService.getLoginUser(request).getId();
        List<FriendRequest> friendRequestList = friendRequestMapper.queryFriendRequestList(userId);
        if (CollectionUtils.isEmpty(friendRequestList)) {
            return null;
        }
        log.info("friendRequestList:{}", friendRequestList);
        return friendRequestList;
    }

    /**
     * 查看是否为好友
     *
     * @param requesterId
     * @return
     */
    @Override
    public boolean existFriend(Long userId, Long requesterId) {
        return friendShipMapper.existFriend(userId,requesterId);
    }

    /**
     * 根据id查询好友信息
     *
     * @param userId
     * @param friendId
     * @return
     */
    @Override
    public FriendShip queryFriendById(Long userId, Long friendId) {
        return friendShipMapper.queryFriendById(userId, friendId);
    }


}
