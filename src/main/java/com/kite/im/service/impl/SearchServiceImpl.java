package com.kite.im.service.impl;

import com.kite.im.model.enums.SearchTypeEnum;
import com.kite.im.model.vo.SearchResultVO;
import com.kite.im.service.SearchService;
import com.kite.im.service.search.SearchStrategy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class SearchServiceImpl implements SearchService {

    private final Map<Integer, SearchStrategy> searchStrategyMap;

    @Autowired
    public SearchServiceImpl(List<SearchStrategy> searchStrategies) {
        // 将所有搜索策略注入到Map中，key为搜索类型
        this.searchStrategyMap = searchStrategies.stream()
                .collect(Collectors.toMap(SearchStrategy::getSearchType, Function.identity()));
    }

    @Override
    public List<SearchResultVO> search(String keyword, Integer type) {
        List<SearchResultVO> results = new ArrayList<>();
        
        // 如果是全部搜索，则执行所有搜索策略
        if (SearchTypeEnum.ALL.getType().equals(type)) {
            searchStrategyMap.values().forEach(strategy -> {
                SearchResultVO result = strategy.search(keyword);
                if (result.getTotal() > 0) {
                    results.add(result);
                }
            });
        }
        // 否则执行指定类型的搜索策略
        else {
            SearchStrategy strategy = searchStrategyMap.get(type);
            if (strategy != null) {
                SearchResultVO result = strategy.search(keyword);
                if (result.getTotal() > 0) {
                    results.add(result);
                }
            }
        }
        
        return results;
    }
} 