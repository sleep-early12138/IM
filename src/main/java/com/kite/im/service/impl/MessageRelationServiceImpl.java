package com.kite.im.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.kite.im.constant.MessageConstant;
import com.kite.im.mapper.MessageRelationMapper;
import com.kite.im.model.entity.MessageRelation;
import com.kite.im.service.MessageRelationService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Slf4j
@Service
public class MessageRelationServiceImpl extends ServiceImpl<MessageRelationMapper, MessageRelation> implements MessageRelationService {

    @Override
    @Transactional
    public void createMessageRelation(Long messageId, Long userId, String conversationId, boolean isSender) {
        MessageRelation relation = new MessageRelation();
        relation.setMessageId(messageId);
        relation.setUserId(userId);
        relation.setConversationId(conversationId);
        relation.setIsSender(isSender);
        relation.setStatus(isSender ? MessageConstant.MESSAGE_STATUS_READ : MessageConstant.MESSAGE_STATUS_UNREAD);
        relation.setCreateTime(System.currentTimeMillis());
        relation.setUpdateTime(System.currentTimeMillis());
        this.save(relation);
    }

    @Override
    @Transactional
    public void batchCreateMessageRelation(List<MessageRelation> relations) {
        this.saveBatch(relations);
    }

    @Override
    @Transactional
    public void updateMessageStatus(Long messageId, Long userId, Integer status) {
        this.lambdaUpdate()
                .eq(MessageRelation::getMessageId, messageId)
                .eq(MessageRelation::getUserId, userId)
                .set(MessageRelation::getStatus, status)
                .set(MessageRelation::getUpdateTime, System.currentTimeMillis())
                .update();
    }

    @Override
    public List<MessageRelation> getMessageRelations(String conversationId, Long userId) {
        return this.lambdaQuery()
                .eq(MessageRelation::getConversationId, conversationId)
                .eq(MessageRelation::getUserId, userId)
                .list();
    }

    @Override
    @Transactional
    public void deleteMessageRelation(Long messageId, Long userId) {
        this.lambdaUpdate()
                .eq(MessageRelation::getMessageId, messageId)
                .eq(MessageRelation::getUserId, userId)
                .set(MessageRelation::getStatus, MessageConstant.MESSAGE_STATUS_DELETE)
                .set(MessageRelation::getUpdateTime, System.currentTimeMillis())
                .update();
    }

    @Override
    @Transactional
    public void markMessageRead(String conversationId, Long userId, Long readTime) {
        this.lambdaUpdate()
                .eq(MessageRelation::getConversationId, conversationId)
                .eq(MessageRelation::getUserId, userId)
                .eq(MessageRelation::getStatus, MessageConstant.MESSAGE_STATUS_UNREAD)
                .set(MessageRelation::getStatus, MessageConstant.MESSAGE_STATUS_READ)
                .set(MessageRelation::getReadTime, readTime)
                .set(MessageRelation::getUpdateTime, System.currentTimeMillis())
                .update();
    }
} 