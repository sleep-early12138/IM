package com.kite.im.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.kite.im.model.entity.Event;
import com.kite.im.service.EventService;
import com.kite.im.mapper.EventMapper;
import org.springframework.stereotype.Service;

/**
* @author lizhouwei
* @description 针对表【event(event)】的数据库操作Service实现
* @createDate 2023-06-20 20:26:25
*/
@Service
public class EventServiceImpl extends ServiceImpl<EventMapper, Event>
    implements EventService{

}




