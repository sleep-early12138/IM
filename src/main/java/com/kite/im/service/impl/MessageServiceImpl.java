package com.kite.im.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.kite.im.constant.MessageConstant;
import com.kite.im.controller.MessageSseController;
import com.kite.im.mapper.MessageMapper;
import com.kite.im.model.entity.Message;
import com.kite.im.model.entity.MessageRelation;
import com.kite.im.model.vo.ChatMessageVO;
import com.kite.im.service.ConversationService;
import com.kite.im.service.MessageRelationService;
import com.kite.im.service.MessageService;
import com.kite.im.utils.JSONUtil;
import com.kite.im.websocket.WebSocketServer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.util.List;

@Slf4j
@Service
public class MessageServiceImpl extends ServiceImpl<MessageMapper, Message> implements MessageService {

    @Autowired
    private MessageRelationService messageRelationService;

    @Autowired
    private ConversationService conversationService;

    @Autowired
    private MessageSseController messageSseController;

    @PostConstruct
    public void init() {
        WebSocketServer.setMessageService(this);
        WebSocketServer.setConversationService(conversationService);
    }

    @Override
    @Transactional
    public void sendMessage(Message message) {
        // 1. 保存消息
        message.setCreateTime(System.currentTimeMillis());
        message.setUpdateTime(System.currentTimeMillis());
        message.setSendTime(System.currentTimeMillis());
        message.setStatus(MessageConstant.MESSAGE_STATUS_UNREAD);
        this.save(message);

        // 2. 创建或获取会话
        String conversationId = conversationService.createOrGetConversation(
                message.getSenderId(),
                message.getReceiverId(),
                message.getConversationType()
        ).getId();

        // 3. 创建消息关系
        messageRelationService.createMessageRelation(message.getId(), message.getSenderId(), conversationId, true);
        messageRelationService.createMessageRelation(message.getId(), message.getReceiverId(), conversationId, false);

        // 4. 更新会话最后一条消息
        conversationService.updateLastMessage(conversationId, message.getId(), message.getContent(), message.getSendTime());

        // 5. 更新未读数
        conversationService.updateUnreadCount(conversationId, message.getReceiverId(), true);

        // 6. 构建消息VO并通过SSE推送给接收者
        ChatMessageVO messageVO = new ChatMessageVO();
        messageVO.setMessageId(message.getId());
        messageVO.setType(1); // 普通消息
        messageVO.setConversationId(conversationId);
        messageVO.setSenderId(message.getSenderId());
        messageVO.setReceiverId(message.getReceiverId());
        messageVO.setContent(message.getContent());
        messageVO.setContentType(message.getContentType());
        messageVO.setSendTime(message.getSendTime());
        messageVO.setExtra(message.getExtra());
        messageVO.setSequence(message.getSequence());

        messageSseController.sendMessage(message.getReceiverId(), messageVO);
    }

    @Override
    public Page<Message> getMessageHistory(String conversationId, Long userId, Long lastMessageId, Integer pageSize) {
        // 获取用户在该会话中的消息关系
        List<MessageRelation> relations = messageRelationService.getMessageRelations(conversationId, userId);
        if (relations.isEmpty()) {
            return new Page<>();
        }

        // 构建消息ID列表
        List<Long> messageIds = relations.stream()
                .map(MessageRelation::getMessageId)
                .collect(java.util.stream.Collectors.toList());

        // 分页查询消息
        Page<Message> page = new Page<>(1, pageSize);
        LambdaQueryWrapper<Message> wrapper = new LambdaQueryWrapper<Message>()
                .in(Message::getId, messageIds)
                .lt(lastMessageId != null, Message::getId, lastMessageId)
                .orderByAsc(Message::getSendTime);

        return this.page(page, wrapper);
    }

    @Override
    @Transactional
    public void markMessageRead(String conversationId, Long userId) {
        long currentTime = System.currentTimeMillis();
        // 更新消息关系的已读时间
        messageRelationService.markMessageRead(conversationId, userId, currentTime);
        // 更新会话未读数
        conversationService.updateUnreadCount(conversationId, userId, false);
    }

    @Override
    @Transactional
    public void recallMessage(Long messageId, Long userId) {
        Message message = this.getById(messageId);
        if (message != null && message.getSenderId().equals(userId)) {
            message.setStatus(MessageConstant.MESSAGE_STATUS_RECALL);
            this.updateById(message);
        }
    }

    @Override
    @Transactional
    public void deleteMessage(Long messageId, Long userId) {
        messageRelationService.deleteMessageRelation(messageId, userId);
    }

    @Override
    public Integer getUnreadCount(Long userId) {
        return messageRelationService.lambdaQuery()
                .eq(MessageRelation::getUserId, userId)
                .eq(MessageRelation::getStatus, MessageConstant.MESSAGE_STATUS_UNREAD)
                .count().intValue();
    }

    @Override
    public Message getLastMessage(String conversationId) {
        List<MessageRelation> relations = messageRelationService.lambdaQuery()
                .eq(MessageRelation::getConversationId, conversationId)
                .orderByDesc(MessageRelation::getMessageId)
                .last("LIMIT 1")
                .list();

        if (!relations.isEmpty()) {
            return this.getById(relations.get(0).getMessageId());
        }
        return null;
    }
}