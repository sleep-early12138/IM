package com.kite.im.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.kite.im.constant.MessageConstant;
import com.kite.im.mapper.ConversationMapper;
import com.kite.im.mapper.UserMapper;
import com.kite.im.model.entity.Conversation;
import com.kite.im.model.entity.User;
import com.kite.im.model.vo.ConversationVO;
import com.kite.im.service.ConversationService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

@Slf4j
@Service
public class ConversationServiceImpl extends ServiceImpl<ConversationMapper, Conversation> implements ConversationService {

    @Autowired
    private UserMapper userMapper;

    @Override
    @Transactional
    public Conversation createOrGetConversation(Long userId, Long targetId, Integer type) {
        // 生成会话ID（确保双方看到的是同一个会话）
        String conversationId = generateConversationId(userId, targetId, type);
        
        // 查询是否已存在会话
        Conversation conversation = this.getById(conversationId);
        if (conversation != null) {
            return conversation;
        }

        // 创建新会话
        conversation = new Conversation();
        conversation.setId(conversationId);
        conversation.setType(type);
        conversation.setOwnerId(userId);
        conversation.setTargetId(targetId);
        conversation.setStatus(MessageConstant.CONVERSATION_STATUS_NORMAL);
        conversation.setUnreadCount(0);
        conversation.setCreateTime(System.currentTimeMillis());
        conversation.setUpdateTime(System.currentTimeMillis());
        this.save(conversation);

        return conversation;
    }

    @Override
    public List<ConversationVO> getConversationList(Long userId) {
        List<Conversation> listMyCreate = this.lambdaQuery()
                .eq(Conversation::getOwnerId, userId)
                .orderByDesc(Conversation::getLastMessageTime)
                .list();
        List<ConversationVO> conversationVO1 = this.convertToVO(listMyCreate, 1);
        List<Conversation> listTargetCreate = this.lambdaQuery()
                .eq(Conversation::getTargetId, userId)
                .orderByDesc(Conversation::getLastMessageTime)
                .list();
        List<ConversationVO> conversationVO2 = this.convertToVO(listTargetCreate, 2);

        //组合两个列表
        List<ConversationVO> list = new ArrayList<>();
        list.addAll(conversationVO1);
        list.addAll(conversationVO2);
        return list;
    }


    @Override
    @Transactional
    public void updateLastMessage(String conversationId, Long messageId, String content, Long messageTime) {
        this.lambdaUpdate()
                .eq(Conversation::getId, conversationId)
                .set(Conversation::getLastMessageId, messageId)
                .set(Conversation::getLastMessageContent, content)
                .set(Conversation::getLastMessageTime, messageTime)
                .set(Conversation::getUpdateTime, System.currentTimeMillis())
                .update();
    }

    @Override
    @Transactional
    public void updateUnreadCount(String conversationId, Long userId, boolean isIncrement) {
        if (isIncrement) {
            this.lambdaUpdate()
                    .eq(Conversation::getId, conversationId)
                    .eq(Conversation::getOwnerId, userId)
                    .setSql("unread_count = unread_count + 1")
                    .update();
        } else {
            this.lambdaUpdate()
                    .eq(Conversation::getId, conversationId)
                    .eq(Conversation::getOwnerId, userId)
                    .set(Conversation::getUnreadCount, 0)
                    .update();
        }
    }

    @Override
    @Transactional
    public void deleteConversation(String conversationId, Long userId) {
        this.lambdaUpdate()
                .eq(Conversation::getId, conversationId)
                .eq(Conversation::getOwnerId, userId)
                .set(Conversation::getStatus, MessageConstant.CONVERSATION_STATUS_HIDE)
                .update();
    }

    @Override
    @Transactional
    public void pinConversation(String conversationId, Long userId) {
        this.lambdaUpdate()
                .eq(Conversation::getId, conversationId)
                .eq(Conversation::getOwnerId, userId)
                .set(Conversation::getStatus, MessageConstant.CONVERSATION_STATUS_TOP)
                .update();
    }

    @Override
    @Transactional
    public void unpinConversation(String conversationId, Long userId) {
        this.lambdaUpdate()
                .eq(Conversation::getId, conversationId)
                .eq(Conversation::getOwnerId, userId)
                .set(Conversation::getStatus, MessageConstant.CONVERSATION_STATUS_NORMAL)
                .update();
    }

    /**
     * 生成会话ID
     * 规则：单聊 - single_{minId}_{maxId}
     *      群聊 - group_{groupId}
     */
    private String generateConversationId(Long userId, Long targetId, Integer type) {
        if (type == MessageConstant.CONVERSATION_TYPE_SINGLE) {
            Long minId = Math.min(userId, targetId);
            Long maxId = Math.max(userId, targetId);
            return String.format("single_%d_%d", minId, maxId);
        } else {
            return String.format("group_%d", targetId);
        }
    }


    /**
     *
     * @param list
     * @return
     */
    private List<ConversationVO> convertToVO(List<Conversation> list, int type) {
       Long queryId;
        // 单聊
        // 群聊
        List<ConversationVO> result = new ArrayList<>();
        for (Conversation conversation : list) {
            ConversationVO vo = new ConversationVO();
            vo.setId(conversation.getId());
            if (conversation.getType() == MessageConstant.CONVERSATION_TYPE_SINGLE) {
                if (type == 1) {
                    queryId = conversation.getTargetId();
                } else {
                    queryId = conversation.getOwnerId();
                }
                vo.setReciverId(queryId);
                User user = userMapper.selectById(queryId);
                // 单聊
                vo.setUsername(user.getUserName());
                vo.setAvatar(user.getUserAvatar());
            } else {
                // 群聊
                vo.setUsername("");
                vo.setAvatar("");
            }
            vo.setOnline("");
            vo.setUnreadCount(String.valueOf(conversation.getUnreadCount()));
            vo.setLastMessage(conversation.getLastMessageContent());
            vo.setLastMessageTime(String.valueOf(conversation.getLastMessageTime()));

            ConversationVO apply = vo;
            result.add(apply);
        }
        return result;
    }
} 