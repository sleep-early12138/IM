package com.kite.im.service;

import com.kite.im.model.entity.Certificate;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 数字证书服务接口
 */
public interface CertificateService extends IService<Certificate> {
    
    /**
     * 生成CA根证书
     * @return CA根证书
     */
    Certificate generateCARootCertificate();
    
    /**
     * 为用户生成数字证书
     * @param userId 用户ID
     * @param userName 用户名称
     * @param publicKey 用户公钥
     * @return 用户证书
     */
    Certificate generateUserCertificate(Long userId, String userName, String publicKey);
    
    /**
     * 验证证书有效性
     * @param certificate 证书对象
     * @return 是否有效
     */
    boolean verifyCertificate(Certificate certificate);
    
    /**
     * 根据用户ID获取证书
     * @param userId 用户ID
     * @return 证书对象
     */
    Certificate getCertificateByUserId(Long userId);
    
    /**
     * 根据证书序列号获取证书
     * @param serialNumber 证书序列号
     * @return 证书对象
     */
    Certificate getCertificateBySerialNumber(String serialNumber);
    
    /**
     * 吊销证书
     * @param serialNumber 证书序列号
     * @return 是否成功
     */
    boolean revokeCertificate(String serialNumber);
}