package com.kite.im.service.search;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.kite.im.model.enums.SearchTypeEnum;
import com.kite.im.model.entity.User;
import com.kite.im.model.vo.SearchResultVO;
import com.kite.im.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class UserSearchStrategy implements SearchStrategy {

    @Autowired
    private UserService userService;

    @Override
    public SearchResultVO search(String keyword) {
        LambdaQueryWrapper<User> queryWrapper = new LambdaQueryWrapper<User>()
                .like(User::getUserName, keyword)
                .or()
                .like(User::getUserAccount, keyword);

        List<User> users = userService.list(queryWrapper);

        SearchResultVO result = new SearchResultVO();
        result.setType(getSearchType());
        result.setTypeDesc(SearchTypeEnum.USER.getDesc());
        result.setTotal((long) users.size());
        result.setItems(users.stream().map(Object.class::cast).collect(Collectors.toList()));
        return result;
    }

    @Override
    public Integer getSearchType() {
        return SearchTypeEnum.USER.getType();
    }
} 