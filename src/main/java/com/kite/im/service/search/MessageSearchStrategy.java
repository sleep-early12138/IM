package com.kite.im.service.search;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.kite.im.model.enums.SearchTypeEnum;
import com.kite.im.model.entity.Message;
import com.kite.im.model.vo.SearchResultVO;
import com.kite.im.service.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class MessageSearchStrategy implements SearchStrategy {

    @Autowired
    private MessageService messageService;

    @Override
    public SearchResultVO search(String keyword) {
        LambdaQueryWrapper<Message> queryWrapper = new LambdaQueryWrapper<Message>()
                .like(Message::getContent, keyword)
                .orderByDesc(Message::getSendTime);

        List<Message> messages = messageService.list(queryWrapper);

        SearchResultVO result = new SearchResultVO();
        result.setType(getSearchType());
        result.setTypeDesc(SearchTypeEnum.MESSAGE.getDesc());
        result.setTotal((long) messages.size());
        result.setItems(messages.stream().map(Object.class::cast).collect(Collectors.toList()));
        return result;
    }

    @Override
    public Integer getSearchType() {
        return SearchTypeEnum.MESSAGE.getType();
    }
} 