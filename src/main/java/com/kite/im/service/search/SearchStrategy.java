package com.kite.im.service.search;

import com.kite.im.model.vo.SearchResultVO;

public interface SearchStrategy {
    /**
     * 执行搜索
     *
     * @param keyword 搜索关键词
     * @return 搜索结果
     */
    SearchResultVO search(String keyword);

    /**
     * 获取搜索类型
     *
     * @return 搜索类型
     */
    Integer getSearchType();
} 