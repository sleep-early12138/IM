package com.kite.im.service.search;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.kite.im.model.enums.SearchTypeEnum;
import com.kite.im.model.entity.Conversation;
import com.kite.im.model.vo.SearchResultVO;
import com.kite.im.service.ConversationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ConversationSearchStrategy implements SearchStrategy {

    @Autowired
    private ConversationService conversationService;

    @Override
    public SearchResultVO search(String keyword) {
        LambdaQueryWrapper<Conversation> queryWrapper = new LambdaQueryWrapper<Conversation>()
                .like(Conversation::getName, keyword)
                .or()
                .like(Conversation::getLastMessageContent, keyword)
                .orderByDesc(Conversation::getLastMessageTime);

        List<Conversation> conversations = conversationService.list(queryWrapper);

        SearchResultVO result = new SearchResultVO();
        result.setType(getSearchType());
        result.setTypeDesc(SearchTypeEnum.CONVERSATION.getDesc());
        result.setTotal((long) conversations.size());
        result.setItems(conversations.stream().map(Object.class::cast).toList());
        return result;
    }

    @Override
    public Integer getSearchType() {
        return SearchTypeEnum.CONVERSATION.getType();
    }
} 