package com.kite.im.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.kite.im.model.entity.SystemNotice;

import java.util.List;

/**
 * 系统通知服务接口
 */
public interface SystemNoticeService extends IService<SystemNotice> {

    /**
     * 创建系统通知
     *
     * @param systemNotice 系统通知信息
     * @return 创建成功返回true，否则返回false
     */
    boolean createNotice(SystemNotice systemNotice);

    /**
     * 更新系统通知
     *
     * @param systemNotice 系统通知信息
     * @return 更新成功返回true，否则返回false
     */
    boolean updateNotice(SystemNotice systemNotice);

    /**
     * 删除系统通知
     *
     * @param noticeId 通知ID
     * @return 删除成功返回true，否则返回false
     */
    boolean deleteNotice(Long noticeId);

    /**
     * 批量删除系统通知
     *
     * @param noticeIds 通知ID列表
     * @return 删除成功返回true，否则返回false
     */
    boolean batchDeleteNotices(List<Long> noticeIds);

    /**
     * 根据ID获取系统通知
     *
     * @param noticeId 通知ID
     * @return 系统通知信息
     */
    SystemNotice getNoticeById(Long noticeId);

    /**
     * 分页获取用户的系统通知
     *
     * @param receiverId 接收者ID
     * @param page 页码
     * @param size 每页大小
     * @return 分页的系统通知列表
     */
    IPage<SystemNotice> getUserNotices(Long receiverId, long page, long size);

    /**
     * 将通知标记为已读
     *
     * @param noticeId 通知ID
     * @return 更新成功返回true，否则返回false
     */
    boolean markAsRead(Long noticeId);

    /**
     * 批量将通知标记为已读
     *
     * @param noticeIds 通知ID列表
     * @return 更新成功返回true，否则返回false
     */
    boolean batchMarkAsRead(List<Long> noticeIds);

    /**
     * 获取用户未读通知数量
     *
     * @param receiverId 接收者ID
     * @return 未读通知数量
     */
    long getUnreadCount(Long receiverId);
}