package com.kite.im.service;

import com.kite.im.model.entity.Message;
import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import java.util.List;

public interface MessageService extends IService<Message> {
    
    /**
     * 发送消息
     */
    void sendMessage(Message message);
    
    /**
     * 获取会话消息历史
     */
    Page<Message> getMessageHistory(String conversationId, Long userId, Long lastMessageId, Integer pageSize);
    
    /**
     * 标记消息已读
     */
    void markMessageRead(String conversationId, Long userId);
    
    /**
     * 撤回消息
     */
    void recallMessage(Long messageId, Long userId);
    
    /**
     * 删除消息
     */
    void deleteMessage(Long messageId, Long userId);
    
    /**
     * 获取未读消息数
     */
    Integer getUnreadCount(Long userId);
    
    /**
     * 获取会话最后一条消息
     */
    Message getLastMessage(String conversationId);
} 