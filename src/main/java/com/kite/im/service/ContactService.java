package com.kite.im.service;


import com.kite.im.model.entity.FriendRequest;
import com.kite.im.model.entity.FriendShip;
import com.kite.im.model.param.audit.QueryFriendParam;
import com.kite.im.model.query.ContactQuery;
import io.swagger.models.auth.In;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author: lizhouwei
 * @ date : 2023/5/29
 * @description:
 */
public interface ContactService  {



    QueryFriendParam queryByUserAccount(String userAccount, HttpServletRequest request);

    /**
     * 发起好友申请
     *
     * @param friendSecretIdentify
     * @param reason
     * @param request
     * @return
     */
    Long requestFriend(String friendSecretIdentify, String reason, HttpServletRequest request);

    int requestFriendAudit(Long requestId, Long audiId,HttpServletRequest request);


    List<ContactQuery> queryFriendList(HttpServletRequest request);

    int deleteFriend(Long friendId, HttpServletRequest request);

    List<FriendRequest> queryFriendRequestList(HttpServletRequest request);


    boolean existFriend(Long userId, Long requesterId);

    FriendShip queryFriendById(Long userId, Long friendId);
}
