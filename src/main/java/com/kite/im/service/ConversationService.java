package com.kite.im.service;

import com.kite.im.model.entity.Conversation;
import com.baomidou.mybatisplus.extension.service.IService;
import com.kite.im.model.vo.ConversationVO;

import java.util.List;

public interface ConversationService extends IService<Conversation> {
    
    /**
     * 创建或获取会话
     */
    Conversation createOrGetConversation(Long userId, Long targetId, Integer type);
    
    /**
     * 获取用户的会话列表
     */
    List<ConversationVO> getConversationList(Long userId);
    
    /**
     * 更新会话最后一条消息
     */
    void updateLastMessage(String conversationId, Long messageId, String content, Long messageTime);
    
    /**
     * 更新会话未读数
     */
    void updateUnreadCount(String conversationId, Long userId, boolean isIncrement);
    
    /**
     * 删除会话
     */
    void deleteConversation(String conversationId, Long userId);
    
    /**
     * 置顶会话
     */
    void pinConversation(String conversationId, Long userId);
    
    /**
     * 取消置顶会话
     */
    void unpinConversation(String conversationId, Long userId);
} 