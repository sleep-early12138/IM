package com.kite.im.service;

import com.kite.im.model.entity.MessageRelation;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;

public interface MessageRelationService extends IService<MessageRelation> {
    
    /**
     * 创建消息关系
     */
    void createMessageRelation(Long messageId, Long userId, String conversationId, boolean isSender);
    
    /**
     * 批量创建消息关系
     */
    void batchCreateMessageRelation(List<MessageRelation> relations);
    
    /**
     * 更新消息状态
     */
    void updateMessageStatus(Long messageId, Long userId, Integer status);
    
    /**
     * 获取用户在会话中的消息关系
     */
    List<MessageRelation> getMessageRelations(String conversationId, Long userId);
    
    /**
     * 删除消息关系
     */
    void deleteMessageRelation(Long messageId, Long userId);
    
    /**
     * 标记消息已读
     */
    void markMessageRead(String conversationId, Long userId, Long readTime);
} 