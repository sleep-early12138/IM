package com.kite.im.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.kite.im.model.entity.Event;

/**
* @author lizhouwei
* @description 针对表【event(event)】的数据库操作Service
* @createDate 2023-06-20 20:26:25
*/
public interface EventService extends IService<Event> {

}
