package com.kite.im.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.kite.im.model.entity.Qun;
import com.kite.im.model.param.qun.InviteFriendParam;
import com.kite.im.model.param.qun.ModifyQunParam;
import com.kite.im.model.param.qun.QunCreateParam;
import com.kite.im.model.param.qun.ReplyInviteParam;
import com.kite.im.model.vo.QunVO;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
* @author lizhouwei
* @description 针对表【qun(qun)】的数据库操作Service
* @createDate 2023-06-20 20:26:25
*/
public interface QunService extends IService<Qun> {

    /**
     * 创建群聊
     * @param qunCreateParam
     * @return
     */
    Long newQun(QunCreateParam qunCreateParam);

    /**
     * 修改群聊信息
     * @param modifyQunParam
     * @return
     */
    Long modifyQun(ModifyQunParam modifyQunParam);

    /**
     * 查询群聊详情
     * @param qunId
     * @return
     */
    QunVO queryQunDetail(Long qunId);

    /**
     * 查询群聊列表
     * @param categoryId
     * @return
     */
    List<QunVO> queryQunList(Long categoryId);

    /**
     * 查询我创建的群聊列表
     * @param request
     * @return
     */
    List<QunVO> queryMyCreateQunList(HttpServletRequest request);

    /**
     * 查询我加入的群聊列表
     * @param request
     * @return
     */
    List<QunVO> queryMyJoinQunList(HttpServletRequest request);

    /**
     * 生成二维码
     * @param qunId
     * @return
     */
    byte[] generateQRCode(Long qunId);

    /**
     * 邀请好友加入群聊
     * @param inviteFriendParam
     * @param request
     * @return
     */
    Object inviteFriend(InviteFriendParam inviteFriendParam,HttpServletRequest request);

    /**
     * 扫码加入群聊
     * @param base64code
     * @return
     */
    Long joinQunByQRCode(String base64code);

//    /**
//     * 审核入群申请
//     * @param applyId
//     * @param operatorId
//     * @param approved
//     * @param request
//     * @return
//     */
//    Object auditQun(Integer applyId, Integer operatorId, Boolean approved,HttpServletRequest request);
//
//    /**
//     * 申请加入群聊
//     * @param qunId
//     * @param request
//     * @return
//     */
//    Object applyJoinQun(Integer qunId,HttpServletRequest request);

    /**
     * 回复入群邀请
     * @param replyInviteParam
     * @param request
     * @return
     */
    Object replyInviteQun(ReplyInviteParam replyInviteParam, HttpServletRequest request);

    /**
     * 解散群聊
     * @param qunId
     * @return
     */
    Object dismissQun(Long qunId,HttpServletRequest request);
}
