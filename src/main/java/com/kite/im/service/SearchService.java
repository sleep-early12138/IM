package com.kite.im.service;

import com.kite.im.model.vo.SearchResultVO;

import java.util.List;

public interface SearchService {
    /**
     * 聚合搜索
     *
     * @param keyword 搜索关键词
     * @param type 搜索类型
     * @return 搜索结果列表
     */
    List<SearchResultVO> search(String keyword, Integer type);
} 