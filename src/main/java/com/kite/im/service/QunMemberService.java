package com.kite.im.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.kite.im.model.entity.Qun;
import com.kite.im.model.entity.QunMember;
import com.kite.im.model.param.qun.RemoveMemberParam;
import com.kite.im.model.vo.QunMemberVO;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
* @author lizhouwei
* @description 针对表【qun_member(qun_member)】的数据库操作Service
* @createDate 2023-06-20 20:26:25
*/
public interface QunMemberService extends IService<QunMember> {

    /**
     * 退出群聊
     * @param qunId
     * @return
     */
    Object quitQun(Long qunId, HttpServletRequest request);

    /**
     * 踢出群聊
     * @param removeMemberParam
     * @return
     */
    Object kickOutQun(RemoveMemberParam removeMemberParam);

    /**
     * 群主转让
     * @param qunId
     * @param newOwner
     * @return
     */
    Object transferQun(Long qunId, Long newOwner);

    /**
     * 查询群成员列表
     * @param qunId
     * @return
     */
    List<QunMemberVO> queryQunMemberList(Long qunId);

}
