package com.kite.im.model.query;

import lombok.Data;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * 用户登录请求体
 *
 * @author lizhowuei
 */
@Data
public class UserLoginQuery implements Serializable {

    /**
     * 用户ID
     */
    private Integer userId;
    /**
     *
     * 用户昵称
     */
    private String nickName;

    /**
     * E-MAIL
     */
    private String email;

    /**
     * 密码
     */
    private String password;
    /**
     * 用户名
     */
    private String userName;
    /**
     * 头象
     */
    private String avatar;
    /**
     * 设备id
     */
    private String deviceId;

    private Integer days;

    private Long expireAt;

    private Map<String, Object> extensions = new LinkedHashMap<>();


}
