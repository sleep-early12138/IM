package com.kite.im.model.query;


import com.kite.im.model.entity.User;

/**
 * @author: lizhouwei
 * @ date : 2023/5/30
 * @description:
 */
public class ContactQuery extends User {

    /**
     * 昵称
     */
    private String nickName;

    /**
     * 头像
     */
    private String avatar;

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

}
