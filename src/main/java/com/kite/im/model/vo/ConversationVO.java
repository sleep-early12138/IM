package com.kite.im.model.vo;

import com.kite.im.model.entity.Conversation;
import lombok.Data;


@Data
public class ConversationVO  {
    private String id;
    private String username;
    private String avatar;
    private String online;
    private String unreadCount;
    private String lastMessage;
    private String lastMessageTime;
    private Long reciverId;
}