package com.kite.im.model.vo;

import lombok.Data;

@Data
public class QunVO {
    private Long qunId;//群ID
    private String name;//群名称
    private String announcement;//群公告
    private String nationality;//所在国籍
    //private Long organizationId;//所在组织
    private String remark;//备注
    private Long ownerId;//群主
    private Long ownerName;//群主Name
    private Long categoryId;//类型
    private String categoryName;//类型名称
}