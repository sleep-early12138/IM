package com.kite.im.model.vo;

import lombok.Data;

@Data
public class FriendRequestVO {

    /**
     * 昵称
     */
    private String nickName;

    /**
     * 头像
     */
    private String avatar;
}
