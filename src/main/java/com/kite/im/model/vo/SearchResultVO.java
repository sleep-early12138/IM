package com.kite.im.model.vo;

import lombok.Data;

import java.util.List;

@Data
public class SearchResultVO {
    /**
     * 搜索结果类型
     */
    private Integer type;

    /**
     * 搜索结果类型描述
     */
    private String typeDesc;

    /**
     * 搜索结果总数
     */
    private Long total;

    /**
     * 搜索结果列表
     */
    private List<Object> items;
} 