package com.kite.im.model.vo;

import lombok.Data;

/**
 * @author: lizhouwei
 * @ date : 2023/6/24
 * @description:
 */
@Data
public class QunMemberVO {

    /**
     * id
     */
    private Long id;

    /**
     * 用户昵称
     */
    private String userName;

    /**
     * 用户头像
     */
    private String userAvatar;
}
