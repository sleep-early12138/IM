package com.kite.im.model.vo;

import lombok.Data;

@Data
public class ChatMessageVO {
    /**
     * 消息ID
     */
    private Long messageId;
    
    /**
     * 消息类型
     */
    private Integer type;
    
    /**
     * 会话ID
     */
    private String conversationId;
    
    /**
     * 发送者ID
     */
    private Long senderId;
    
    /**
     * 接收者ID
     */
    private Long receiverId;
    
    /**
     * 消息内容
     */
    private String content;
    
    /**
     * 消息内容类型
     */
    private Integer contentType;
    
    /**
     * 发送时间
     */
    private Long sendTime;
    
    /**
     * 额外信息
     */
    private String extra;
    
    /**
     * 序列号
     */
    private Long sequence;
} 