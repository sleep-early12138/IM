package com.kite.im.model.dto;

import lombok.Data;

@Data
public class ChatMessageDTO {
    /**
     * 消息类型：
     * 1: 普通消息
     * 2: 系统消息
     * 3: 通知消息
     */
    private Integer type;
    
    /**
     * 会话ID
     */
    private String conversationId;
    
    /**
     * 消息内容
     */
    private String content;
    
    /**
     * 消息内容类型
     * 0: 文本
     * 1: 图片
     * 2: 语音
     * 3: 视频
     * 4: 文件
     */
    private Integer contentType;
    
    /**
     * 接收者ID
     */
    private Long receiverId;
    
    /**
     * 额外信息（JSON格式）
     */
    private String extra;
} 