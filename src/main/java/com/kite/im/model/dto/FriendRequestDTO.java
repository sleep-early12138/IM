package com.kite.im.model.dto;

import lombok.Data;
import org.springframework.web.bind.annotation.RequestParam;

@Data
public class FriendRequestDTO {

    private String   friendSecretIdentify;
    private String   reason;


    private Long requestId;
    private Long auditId;
}
