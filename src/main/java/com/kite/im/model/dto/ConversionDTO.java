package com.kite.im.model.dto;

import lombok.Data;
import org.springframework.web.bind.annotation.RequestParam;

@Data
public class ConversionDTO {

    private  Long targetId;
    private  Integer type;
}
