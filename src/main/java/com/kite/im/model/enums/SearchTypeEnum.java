package com.kite.im.model.enums;

import lombok.Getter;

@Getter
public enum SearchTypeEnum {
    USER(0, "联系人搜索"),
    CONVERSATION(1, "会话搜索"),
    MESSAGE(2, "消息搜索"),
    ALL(3, "全部搜索");

    private final Integer type;
    private final String desc;

    SearchTypeEnum(Integer type, String desc) {
        this.type = type;
        this.desc = desc;
    }

    public static SearchTypeEnum getByType(Integer type) {
        for (SearchTypeEnum value : SearchTypeEnum.values()) {
            if (value.type.equals(type)) {
                return value;
            }
        }
        return ALL;
    }
} 