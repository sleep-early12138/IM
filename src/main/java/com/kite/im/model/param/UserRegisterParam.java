package com.kite.im.model.param;

import lombok.Data;

import java.io.Serializable;

/**
 * 用户注册请求体
 *
 *
 * @author lizhouwei
 */
@Data
public class UserRegisterParam implements Serializable {

    Integer userId;

    /**
     * E-MAIL
     */
    private String email;
    private String userName;

    /**
     * 密码
     */
    private String password;

    private String confirmPassword;
    private String channel;
    private String captch;

}
