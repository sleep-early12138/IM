package com.kite.im.model.param.qun;

import lombok.Data;

import javax.servlet.http.HttpServletRequest;

/**
 * @author: lizhouwei
 * @ date : 2023/6/23
 * @description:
 */
@Data
public class RemoveMemberParam {
    private Long qunId;
    private Long memberId;
    private HttpServletRequest request;
}
