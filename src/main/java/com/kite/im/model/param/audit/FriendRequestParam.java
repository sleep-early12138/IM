package com.kite.im.model.param.audit;


import com.kite.im.model.entity.FriendRequest;
import io.swagger.models.auth.In;
import lombok.Data;

/**
 * @author: lizhouwei
 * @ date : 2023/5/22
 * @description:
 */
@Data
public class FriendRequestParam extends FriendRequest {

    /**
     * 提出申请人id
     */
    private Long requesterId;

    private Long receiverId;

    /**
     * 用户的密秘标识
     */
    private String friendSecretIdentify;
    /**
     * 申请的理由
     */
    private String reason;


}
