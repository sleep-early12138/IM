package com.kite.im.model.param.qun;

import lombok.Data;

/**
 * @author: lizhouwei
 * @ date : 2023/6/23
 * @description:
 */
@Data
public class ReplyInviteParam {
    private  Long qunId;
    private  Long staus;
}
