package com.kite.im.model.param.qun;


import lombok.Data;

@Data
public  class QunCreateParam {
    private String name;//群名称
    private String announcement;//群公告
    private Integer nationalityId;//所在国籍
    private Long organizationId;//所在组织
    private String remark;//备注
    private Long ownerId;//群主
    private Integer categoryId;//类型
}

