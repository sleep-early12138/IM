package com.kite.im.model.param.audit;


import com.kite.im.model.entity.FriendShip;
import lombok.Data;

/**
 * 好友的审核
 */
@Data
public class FriendAuditParam extends FriendShip {
    /**
     * 审核主键ID
     */
    private Integer auditId;

    private Long shipId;


    private Integer requestId;

    /**
     * 审核的状态
     */
    private Integer status;

}
