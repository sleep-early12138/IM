package com.kite.im.model.param.qun;


import lombok.Data;

@Data
public class ModifyQunParam {
    private Long qunId;//群ID
    private String name;//群名称
    private String announcement;//群公告
    private String remark;//备注
}
