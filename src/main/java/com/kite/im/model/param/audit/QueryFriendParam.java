package com.kite.im.model.param.audit;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

/**
 * @author: lizhouwei
 * @ date : 2023/5/29
 * @description:
 */
@Data
public class QueryFriendParam {

    /**
     * 用户的唯一id
     */
    @TableId(type = IdType.AUTO)
    private Integer userId;
    /**
     * 用户的密秘标识
     */
    private String friendSecretIdentify;


    /**
     * 头象
     */
    private String userAvatar;

    /**
     * 昵称
     */
    private String userName;

    /**
     * 签名
     */
    private String personalSignature;

}
