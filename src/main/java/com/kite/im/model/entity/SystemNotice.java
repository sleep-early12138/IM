package com.kite.im.model.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;

/**
 * 系统通知表实体类
 */
@Data
@Accessors(chain = true)
@TableName("system_notice")
public class SystemNotice {

    /**
     * 通知的唯一标识
     */
    @TableId(value = "notice_id", type = IdType.AUTO)
    private Long noticeId;

    /**
     * 通知的发送者ID，若为系统自动发送，此值可为空
     */
    @TableField("sender_id")
    private Long senderId;

    /**
     * 通知的接收者ID
     */
    @TableField("receiver_id")
    private Long receiverId;

    /**
     * 通知的类型，如好友申请、系统公告、活动提醒等
     */
    @TableField("notice_type")
    private String noticeType;

    /**
     * 通知的标题
     */
    @TableField("notice_title")
    private String noticeTitle;

    /**
     * 通知的具体内容
     */
    @TableField("notice_content")
    private String noticeContent;

    /**
     * 通知的发送时间
     */
    @TableField("send_time")
    private LocalDateTime sendTime;

    /**
     * 通知的阅读状态，0表示未读，1表示已读
     */
    @TableField("read_status")
    private Integer readStatus;

    /**
     * 通知的点击状态，0表示未点击，1表示已点击
     */
    @TableField("click_status")
    private Integer clickStatus;

    /**
     * 额外信息，可用于存储与通知相关的其他数据，如跳转链接等
     */
    @TableField("extra_info")
    private String extraInfo;

    /**
     * 创建时间
     */
    @TableField("create_time")
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    @TableField("update_time")
    private LocalDateTime updateTime;

    /**
     * 是否删除
     */
    @TableField("isDelete")
    private Integer isDelete;
}