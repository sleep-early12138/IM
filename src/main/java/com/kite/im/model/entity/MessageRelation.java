package com.kite.im.model.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName("message_relation")
public class MessageRelation {
    @TableId(type = IdType.AUTO)
    private Long id;
    @TableField("message_id")
    private Long messageId;
    @TableField("user_id")
    private Long userId;
    @TableField("conversation_id")
    private String conversationId;
    private Integer status;
    @TableField("is_sender")
    private Boolean isSender;
    @TableField("read_time")
    private Long readTime;
    @TableField("create_time")
    private Long createTime;
    @TableField("update_time")
    private Long updateTime;
} 