package com.kite.im.model.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * 数字证书实体类
 */
@Data
@TableName("certificate")
public class Certificate {
    
    @TableId(type = IdType.AUTO)
    private Long id;
    
    /**
     * 证书序列号
     */
    @TableField("serial_number")
    private String serialNumber;
    
    /**
     * 证书所有者ID
     */
    @TableField("user_id")
    private Long userId;
    
    /**
     * 证书所有者名称
     */
    @TableField("user_name")
    private String userName;
    
    /**
     * 证书公钥
     */
    @TableField("public_key")
    private String publicKey;
    
    /**
     * 证书私钥(CA私钥加密)
     */
    @TableField("private_key")
    private String privateKey;
    
    /**
     * 证书签名
     */
    @TableField("signature")
    private String signature;
    
    /**
     * 证书状态(1:有效 0:吊销)
     */
    @TableField("status")
    private Integer status;
    
    /**
     * 证书有效期开始时间
     */
    @TableField("valid_from")
    private Long validFrom;
    
    /**
     * 证书有效期结束时间
     */
    @TableField("valid_to")
    private Long validTo;
    
    /**
     * 创建时间
     */
    @TableField("create_time")
    private Long createTime;
    
    /**
     * 更新时间
     */
    @TableField("update_time")
    private Long updateTime;
}