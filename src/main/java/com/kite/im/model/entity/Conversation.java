package com.kite.im.model.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName("conversation")
public class Conversation {
    @TableId
    private String id;
    private String name;
    private Integer type;
    @TableField("owner_id")
    private Long ownerId;
    @TableField("target_id")
    private Long targetId;
    @TableField("last_message_id")
    private Long lastMessageId;
    @TableField("last_message_content")
    private String lastMessageContent;
    @TableField("last_message_time")
    private Long lastMessageTime;
    @TableField("unread_count")
    private Integer unreadCount;
    private Integer status;
    @TableField("create_time")
    private Long createTime;
    @TableField("update_time")
    private Long updateTime;
} 