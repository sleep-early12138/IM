package com.kite.im.model.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName("message")
public class Message {
    @TableId(type = IdType.AUTO)
    private Long id;
    @TableField("sender_id")
    private Long senderId;
    @TableField("receiver_id")
    private Long receiverId;
    private String content;
    @TableField("content_type")
    private Integer contentType;
    @TableField("conversation_type")
    private Integer conversationType;
    @TableField("conversation_id")
    private String conversationId;
    private Integer status;
    @TableField("send_time")
    private Long sendTime;
    @TableField("read_time")
    private Long readTime;
    private Long sequence;
    @TableField("is_deleted")
    private Boolean isDeleted;
    private String extra;
    @TableField("create_time")
    private Long createTime;
    @TableField("update_time")
    private Long updateTime;
} 