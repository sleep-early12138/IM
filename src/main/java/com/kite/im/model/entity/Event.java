package com.kite.im.model.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * event
 * @TableName event
 */
@TableName(value ="event")
@Data
public class Event implements Serializable {
    /**
     * primary key
     */
    @TableId(type = IdType.AUTO)
    private Integer eventId;

    /**
     * user id
     */
    private Integer userId;

    /**
     * business typ
     */
    private String businessType;

    /**
     * business id
     */
    private Integer businessId;

    /**
     * times
     */
    private Integer times;

    /**
     * channel
     */
    private String channel;

    /**
     * ip
     */
    private String ip;

    /**
     * device
     */
    private String device;

    /**
     * device unique id
     */
    private String deviceId;

    /**
     * device model 5s e.g ...
     */
    private String deviceModel;

    /**
     * event type
     */
    private String event;

    /**
     * content
     */
    private String content;

    /**
     * website home url
     */
    private String website;

    /**
     * app id
     */
    private Integer appId;

    /**
     * app version
     */
    private Double appVersion;

    /**
     * platform
     */
    private Integer platform;

    /**
     * operation system
     */
    private String os;

    /**
     * use agent
     */
    private String userAgent;

    /**
     * client os version
     */
    private String clientVersion;

    /**
     * longitude
     */
    private Double longitude;

    /**
     * latitude
     */
    private Double latitude;

    /**
     * network
     */
    private String network;

    /**
     * is simulate
     */
    private Integer simulate;

    /**
     * imei
     */
    private String imei;

    /**
     * bssi
     */
    private String bssid;

    /**
     * ssid
     */
    private String ssid;

    /**
     * idfa
     */
    private String idfa;

    /**
     * client start time
     */
    private Long startTime;

    /**
     * client resume time
     */
    private Long resumeTime;

    /**
     * 
     */
    private Long createTime;

    /**
     * 
     */
    private Long updateTime;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        Event other = (Event) that;
        return (this.getEventId() == null ? other.getEventId() == null : this.getEventId().equals(other.getEventId()))
            && (this.getUserId() == null ? other.getUserId() == null : this.getUserId().equals(other.getUserId()))
            && (this.getBusinessType() == null ? other.getBusinessType() == null : this.getBusinessType().equals(other.getBusinessType()))
            && (this.getBusinessId() == null ? other.getBusinessId() == null : this.getBusinessId().equals(other.getBusinessId()))
            && (this.getTimes() == null ? other.getTimes() == null : this.getTimes().equals(other.getTimes()))
            && (this.getChannel() == null ? other.getChannel() == null : this.getChannel().equals(other.getChannel()))
            && (this.getIp() == null ? other.getIp() == null : this.getIp().equals(other.getIp()))
            && (this.getDevice() == null ? other.getDevice() == null : this.getDevice().equals(other.getDevice()))
            && (this.getDeviceId() == null ? other.getDeviceId() == null : this.getDeviceId().equals(other.getDeviceId()))
            && (this.getDeviceModel() == null ? other.getDeviceModel() == null : this.getDeviceModel().equals(other.getDeviceModel()))
            && (this.getEvent() == null ? other.getEvent() == null : this.getEvent().equals(other.getEvent()))
            && (this.getContent() == null ? other.getContent() == null : this.getContent().equals(other.getContent()))
            && (this.getWebsite() == null ? other.getWebsite() == null : this.getWebsite().equals(other.getWebsite()))
            && (this.getAppId() == null ? other.getAppId() == null : this.getAppId().equals(other.getAppId()))
            && (this.getAppVersion() == null ? other.getAppVersion() == null : this.getAppVersion().equals(other.getAppVersion()))
            && (this.getPlatform() == null ? other.getPlatform() == null : this.getPlatform().equals(other.getPlatform()))
            && (this.getOs() == null ? other.getOs() == null : this.getOs().equals(other.getOs()))
            && (this.getUserAgent() == null ? other.getUserAgent() == null : this.getUserAgent().equals(other.getUserAgent()))
            && (this.getClientVersion() == null ? other.getClientVersion() == null : this.getClientVersion().equals(other.getClientVersion()))
            && (this.getLongitude() == null ? other.getLongitude() == null : this.getLongitude().equals(other.getLongitude()))
            && (this.getLatitude() == null ? other.getLatitude() == null : this.getLatitude().equals(other.getLatitude()))
            && (this.getNetwork() == null ? other.getNetwork() == null : this.getNetwork().equals(other.getNetwork()))
            && (this.getSimulate() == null ? other.getSimulate() == null : this.getSimulate().equals(other.getSimulate()))
            && (this.getImei() == null ? other.getImei() == null : this.getImei().equals(other.getImei()))
            && (this.getBssid() == null ? other.getBssid() == null : this.getBssid().equals(other.getBssid()))
            && (this.getSsid() == null ? other.getSsid() == null : this.getSsid().equals(other.getSsid()))
            && (this.getIdfa() == null ? other.getIdfa() == null : this.getIdfa().equals(other.getIdfa()))
            && (this.getStartTime() == null ? other.getStartTime() == null : this.getStartTime().equals(other.getStartTime()))
            && (this.getResumeTime() == null ? other.getResumeTime() == null : this.getResumeTime().equals(other.getResumeTime()))
            && (this.getCreateTime() == null ? other.getCreateTime() == null : this.getCreateTime().equals(other.getCreateTime()))
            && (this.getUpdateTime() == null ? other.getUpdateTime() == null : this.getUpdateTime().equals(other.getUpdateTime()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getEventId() == null) ? 0 : getEventId().hashCode());
        result = prime * result + ((getUserId() == null) ? 0 : getUserId().hashCode());
        result = prime * result + ((getBusinessType() == null) ? 0 : getBusinessType().hashCode());
        result = prime * result + ((getBusinessId() == null) ? 0 : getBusinessId().hashCode());
        result = prime * result + ((getTimes() == null) ? 0 : getTimes().hashCode());
        result = prime * result + ((getChannel() == null) ? 0 : getChannel().hashCode());
        result = prime * result + ((getIp() == null) ? 0 : getIp().hashCode());
        result = prime * result + ((getDevice() == null) ? 0 : getDevice().hashCode());
        result = prime * result + ((getDeviceId() == null) ? 0 : getDeviceId().hashCode());
        result = prime * result + ((getDeviceModel() == null) ? 0 : getDeviceModel().hashCode());
        result = prime * result + ((getEvent() == null) ? 0 : getEvent().hashCode());
        result = prime * result + ((getContent() == null) ? 0 : getContent().hashCode());
        result = prime * result + ((getWebsite() == null) ? 0 : getWebsite().hashCode());
        result = prime * result + ((getAppId() == null) ? 0 : getAppId().hashCode());
        result = prime * result + ((getAppVersion() == null) ? 0 : getAppVersion().hashCode());
        result = prime * result + ((getPlatform() == null) ? 0 : getPlatform().hashCode());
        result = prime * result + ((getOs() == null) ? 0 : getOs().hashCode());
        result = prime * result + ((getUserAgent() == null) ? 0 : getUserAgent().hashCode());
        result = prime * result + ((getClientVersion() == null) ? 0 : getClientVersion().hashCode());
        result = prime * result + ((getLongitude() == null) ? 0 : getLongitude().hashCode());
        result = prime * result + ((getLatitude() == null) ? 0 : getLatitude().hashCode());
        result = prime * result + ((getNetwork() == null) ? 0 : getNetwork().hashCode());
        result = prime * result + ((getSimulate() == null) ? 0 : getSimulate().hashCode());
        result = prime * result + ((getImei() == null) ? 0 : getImei().hashCode());
        result = prime * result + ((getBssid() == null) ? 0 : getBssid().hashCode());
        result = prime * result + ((getSsid() == null) ? 0 : getSsid().hashCode());
        result = prime * result + ((getIdfa() == null) ? 0 : getIdfa().hashCode());
        result = prime * result + ((getStartTime() == null) ? 0 : getStartTime().hashCode());
        result = prime * result + ((getResumeTime() == null) ? 0 : getResumeTime().hashCode());
        result = prime * result + ((getCreateTime() == null) ? 0 : getCreateTime().hashCode());
        result = prime * result + ((getUpdateTime() == null) ? 0 : getUpdateTime().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", eventId=").append(eventId);
        sb.append(", userId=").append(userId);
        sb.append(", businessType=").append(businessType);
        sb.append(", businessId=").append(businessId);
        sb.append(", times=").append(times);
        sb.append(", channel=").append(channel);
        sb.append(", ip=").append(ip);
        sb.append(", device=").append(device);
        sb.append(", deviceId=").append(deviceId);
        sb.append(", deviceModel=").append(deviceModel);
        sb.append(", event=").append(event);
        sb.append(", content=").append(content);
        sb.append(", website=").append(website);
        sb.append(", appId=").append(appId);
        sb.append(", appVersion=").append(appVersion);
        sb.append(", platform=").append(platform);
        sb.append(", os=").append(os);
        sb.append(", userAgent=").append(userAgent);
        sb.append(", clientVersion=").append(clientVersion);
        sb.append(", longitude=").append(longitude);
        sb.append(", latitude=").append(latitude);
        sb.append(", network=").append(network);
        sb.append(", simulate=").append(simulate);
        sb.append(", imei=").append(imei);
        sb.append(", bssid=").append(bssid);
        sb.append(", ssid=").append(ssid);
        sb.append(", idfa=").append(idfa);
        sb.append(", startTime=").append(startTime);
        sb.append(", resumeTime=").append(resumeTime);
        sb.append(", createTime=").append(createTime);
        sb.append(", updateTime=").append(updateTime);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}